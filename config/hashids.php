<?php

/**
 * Copyright (c) Vincent Klaiber.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://github.com/vinkla/laravel-hashids
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Default Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the connections below you wish to use as
    | your default connection for all work. Of course, you may use many
    | connections at once using the manager class.
    |
    */

    'default' => 'main',

    /*
    |--------------------------------------------------------------------------
    | Hashids Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the connections setup for your application. Example
    | configuration has been included, but you may add as many connections as
    | you would like.
    |
    */

    'connections' => [

        'main' => [
            'salt' => 'alammobaito',
            'length' => 8,
            'alphabet' => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
        ],

        'uuid' => [
            'salt' => 'halaoongangmodelongcharing',
            'length' => 16,
            'alphabet' => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
        ],

        'token' => [
            'salt' => 'pinapadalapapo',
            'length' => 6,
            'alphabet' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
        ],

        // Used as Referral Code
        'aid' => [
            'salt' => 'ehdinamanitogamot',
            'length' => 8,
            'alphabet' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
        ],

        'tracking' => [
            'salt' => 'itobaanghanapmongaun',
            'length' => 9,
            'alphabet' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
        ],

    ],

];
