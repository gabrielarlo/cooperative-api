<?php

return [
    'origin' => [
        env('FIREWALL_ORIGIN_WEB', 'WEB-84845'),
        env('FIREWALL_ORIGIN_MOBILE', 'MOBILE-54984'),
        env('FIREWALL_ORIGIN_POSTMAN', 'POSTMAN-00000'),
        env('FIREWALL_ORIGIN_TAB', 'TAB-74185'),
    ],
];
