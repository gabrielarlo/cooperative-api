<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Referral extends Model
{
    use HasFactory;

    public function referral(): BelongsTo
    {
        return $this->belongsTo(Account::class, 'referral_id');
    }

    public function referrer(): BelongsTo
    {
        return $this->belongsTo(Account::class, 'referrer_id');
    }
}
