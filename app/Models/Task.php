<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property mixed creator_id
 * @property mixed category
 * @property mixed title
 * @property mixed description
 * @property mixed link
 * @property mixed has_attachment
 * @property mixed expiration_date
 * @property mixed slots
 * @property mixed reward
 * @property int|mixed status
 * @method static where(string $string, int|null $id)
 * @method static find(int|null $id)
 */
class Task extends Model
{
    use HasFactory;

    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id');
    }
}
