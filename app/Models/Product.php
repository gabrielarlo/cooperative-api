<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function reviews(): array
    {
        $id = $this->attributes['id'];
        $ratings = ProductRating::where('product_id', $id)->sum('rating');
        $reviews = ProductRating::where('product_id', $id)->count();
        return [
            'rating' => $reviews > 0 ? $ratings / $reviews : 0,
            'count' => $reviews,
        ];
    }
}
