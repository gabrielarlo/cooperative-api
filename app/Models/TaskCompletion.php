<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property mixed task_id
 * @property int|mixed|string|null completer_id
 * @property mixed|string attachment_name
 * @property mixed id
 */
class TaskCompletion extends Model
{
    use HasFactory;

    public function completer(): BelongsTo
    {
        return $this->belongsTo(User::class, 'completer_id');
    }
}
