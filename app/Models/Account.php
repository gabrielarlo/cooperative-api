<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Account extends Model
{
    use HasFactory;

    public function referrer(): BelongsTo
    {
        return $this->belongsTo(Account::class, 'referrer_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function level(): HasOne
    {
        return $this->hasOne(Level::class, 'account_id');
    }

    public function referralCount()
    {
        return Account::where('referrer_id', $this->attributes['id'])->where('status', 1)->count();
    }
}
