<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * @method static where(string $string, $email)
 * @method static find(int|string|null $id)
 * @property mixed name
 * @property mixed email
 * @property mixed|string verification_token
 * @property mixed contact
 * @property mixed gender
 * @property mixed|string password
 * @property int|mixed ban
 * @property int|mixed status
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function currentAddress()
    {
        return Address::where('user_id', $this->attributes['id'])->where('status', 1)->where('default', 1)->first();
    }

    public function accounts(): HasMany
    {
        return $this->hasMany(Account::class, 'user_id');
    }
}
