<?php


namespace App\Filters;


use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class ThumbnailFilter implements FilterInterface
{

    public function applyFilter(Image $image): Image
    {
        return $image->resize(320, null, function ($constraints) {
            $constraints->aspectRatio();
        });
    }
}
