<?php


namespace App\Interfaces;


interface ShopInterface
{
    public function saleList($req);

    public function shopList($req);

    public function list($req);

    public function add($req);

    public function update($req);

    public function getImages($req);

    public function uploadImage($req);

    public function deleteImage($req);

    public function cartList($req);

    public function addToCart($req);

    public function subFromCart($req);

    public function removeFromCart($req);

    public function addToCheckout($req);

    public function order($req);

    public function validatePayment($req);

    public function getDeliveryCharges($req);

    public function getCategories();

    public function details($req);

    public function toggleStockStatus($req);

    public function getBanners();

    public function getReviews($req);
}
