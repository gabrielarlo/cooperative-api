<?php


namespace App\Interfaces;


interface AccountInterface
{
    public function create(int $user_id, string $name, int $referrer_id = 0, int $status = 0, string $remarks = '');

    public function getStatus();

    public function getReferralCodes();

    public function users($req);

    public function changeActiveStatus($req);

    public function changeBanStatus($req);
}
