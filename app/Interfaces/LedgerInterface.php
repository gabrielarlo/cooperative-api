<?php


namespace App\Interfaces;


use App\Abstracts\LedgerFlowType;
use App\Abstracts\LedgerStatus;
use App\Models\Ledger;
use App\Models\User;

interface LedgerInterface
{
    public function saveEntry(User $user, int $type, $amount = 0, string $remarks = '', int $status = 0);

    public function mySummary();

    public function summary();

    public function list($req);

    public function general($req);

    public function changeStatus(Ledger $ledger, int $status = LedgerStatus::COMPLETE);
}
