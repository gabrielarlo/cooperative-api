<?php


namespace App\Interfaces;


interface OrderInterface
{
    public function list($req);

    public function myList($req);

    public function updateStatus($req);

    public function productList($req);

    public function productReview($req);
}
