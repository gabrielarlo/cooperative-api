<?php


namespace App\Interfaces;


use App\Models\Level;

interface LevelInterface
{
    public function levelUp(Level $level);

    public function list($req);
}
