<?php


namespace App\Interfaces;


interface AnnouncementInterface
{
    public function list($req);

    public function add($req);

    public function delete($req);
}
