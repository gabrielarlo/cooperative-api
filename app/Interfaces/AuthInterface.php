<?php


namespace App\Interfaces;


interface AuthInterface
{
    public function register($req, bool $internal = false);

    public function login($req);

    public function verifyToken($req);

    public function requestPasswordReset($req);

    public function resetPassword($req);

    public function update($req);

    public function changeAvatar($req);

    public function changeCover($req);

    public function changePassword($req);

    public function activateAccount($req);

    public function toggleBan($req);

    public function logout($req);

    public function checkReferralCode($req);
}
