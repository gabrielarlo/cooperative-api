<?php


namespace App\Interfaces;


interface AddressInterface
{
    public function list();

    public function add($req);

    public function update($req);

    public function delete($req);

    public function makeDefault($req);

    public function getDefault();
}
