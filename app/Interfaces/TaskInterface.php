<?php


namespace App\Interfaces;


interface TaskInterface
{
    public function list($req);

    public function myList($req);

    public function allList($req);

    public function details($req);

    public function create($req);

    public function delete($req);

    public function end($req);

    public function complete($req);

    public function reRun($req);

    public function revoke($req);
}
