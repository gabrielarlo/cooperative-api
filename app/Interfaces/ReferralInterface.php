<?php


namespace App\Interfaces;


use App\Models\Account;

interface ReferralInterface
{
    public function saveNewReferral(Account $referrer, Account $referral, string $remarks = 'To be collected', int $status = 0);

    public function list(?int $user_id);

    public function topTen();
}
