<?php

namespace App\Console\Commands;

use App\Abstracts\LedgerFlowType;
use App\Abstracts\Settings;
use App\Models\Account;
use App\Models\User;
use App\Repositories\LedgerRepository;
use App\Repositories\ReferralRepository;
use Illuminate\Console\Command;

class ReferralCalculator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'referral:calculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculation of Referral Bonuses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        $users = User::all();
//        foreach ($users as $user) {
//            $accounts = Account::where('user_id', $user->id)->where('status', 1)->get();
//            foreach ($accounts as $account) {
//                $referrals = Account::where('referrer_id', $account->id)->where('user_id', '!=', $user->id)->get();
//                dump($user->name, count($referrals));
//
//                if (count($referrals) > 0) {
//                    foreach ($referrals as $referral) {
//                        $result = (new ReferralRepository())->saveNewReferral($account, $referral, 'From Old Record', 1);
//
//                        if ($result) {
//                            (new LedgerRepository())->saveEntry($user, LedgerFlowType::REFERRAL_BONUS, Settings::REFERRAL_BONUS, 'From Old Record', 1);
//                        }
//                    }
//                }
//            }
//        }
    }
}
