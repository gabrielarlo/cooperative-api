<?php

namespace App\Console\Commands;

use App\Abstracts\TaskStatus;
use App\Models\Task;
use App\Repositories\TaskRepository;
use Illuminate\Console\Command;

class CheckTaskStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Task Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Start Checking Tasks');
        $t = Task::where('status', TaskStatus::RUNNING)->get();
        foreach ($t as $task) {
            (new TaskRepository())->getStatus($task);
        }
        $this->info('End...');
    }
}
