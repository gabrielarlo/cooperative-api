<?php

namespace App\Console\Commands;

use App\Abstracts\LedgerFlowType;
use App\Abstracts\Settings;
use App\Models\Account;
use App\Models\Ledger;
use App\Models\Referral;
use App\Models\User;
use App\Repositories\LedgerRepository;
use App\Repositories\ReferralRepository;
use Illuminate\Console\Command;

class ReferralBonusChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'referral:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking the referral bonus';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Start Checking...');

        $a = Account::where('status', 1)->get();
        $user_ids_counts = [];
        foreach ($a as $account) {
            $acc = Account::find($account->referrer_id);
            if ($acc) {
                // Record User
                $count = $user_ids_counts[$acc->user_id] ?? 0;
                $count++;
                $user_ids_counts[$acc->user_id] = $count;

                // TODO: check referrals table
                if ($account->referrer_id > 0) {
                    $r = Referral::where('referrer_id', $account->referrer_id)->where('referral_id', $account->id)->first();
                    if (!$r) {
                        $referrer = Account::find($account->referrer_id);
                        $referral = Account::find($account->id);
                        (new ReferralRepository())->saveNewReferral($referrer, $referral, 'Added by checker', 1);
                        dump('saveNewReferral SAVED');
                    }
                }
            }
        }
        dump($user_ids_counts);
        $all_count = 0;
        foreach ($user_ids_counts as $user_id => $count) {
            $all_count += $count;
            $l_count = Ledger::where('user_id', $user_id)->where('type', LedgerFlowType::REFERRAL_BONUS)->where('status', 1)->count();
            dump($user_id . ' => ' . $count . ' | ' . $l_count);
            if ($l_count < $count) {
                $to_enter = $count - $l_count;
                for ($i = 0; $i < $to_enter; $i++) {
                    $u = User::find($user_id);
                    (new LedgerRepository())->saveEntry($u, LedgerFlowType::REFERRAL_BONUS, Settings::REFERRAL_BONUS, 'New Own referral from checker', 1);
                    dump('saveEntry SAVED');
                }
            } elseif ($l_count > $count) {
                $to_remove = $l_count - $count;
                for ($i = 0; $i < $to_remove; $i++) {
                    $l = Ledger::where('user_id', $user_id)->where('type', LedgerFlowType::REFERRAL_BONUS)->where('status', 1)->orderBy('created_at', 'desc')->first();
                    if ($l) {
                        $l->status = 0;
                        $l->remarks = 'Removed by checker 001';
                        $l->save();
                        dump('Removed SAVED 001');
                    }
                }
            } else {
                $this->line($user_id . ' BALANCED');
            }
        }
        dump($all_count);

        // TODO: check ledgers table
        $this->info('Checking Ledgers');
        $l = Ledger::where('type', LedgerFlowType::REFERRAL_BONUS)->where('status', 1)->get();
        foreach ($l as $ledger) {
            if (array_key_exists($ledger->user_id, $user_ids_counts) == null) {
                $new_l = Ledger::find($ledger->id);
                $new_l->status = 0;
                $new_l->remarks = 'Removed by checker 002';
                $new_l->save();
                dump('Removed SAVED 002');
            }
        }

        $this->info('End');
    }
}
