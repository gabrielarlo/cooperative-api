<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class RenameImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:rename';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rename Images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Start Renaming');
        $paths = config('imagecache.paths');

        foreach ($paths as $path) {
            $found = File::exists($path);
            if ($found) {
                $chunks = explode(DIRECTORY_SEPARATOR, $path);
                $dir = end($chunks);
                $files = File::allFiles($path);
                foreach ($files as $file) {
                    $filename = $file->getFilename();
                    $has = strstr($filename, $dir);
                    if (!$has) {
                        $newName = $dir . '-' . $filename;
//                        dump($file->getRealPath());
//                        dump($file->getPath() . DIRECTORY_SEPARATOR . $newName);
                        File::move($file->getRealPath(), $file->getPath() . DIRECTORY_SEPARATOR . $newName);
                        $this->line('Renaming ' . $filename . ' to ' . $newName);
                    }
                }
            }
        }
        $this->info('Renaming End!');
    }
}
