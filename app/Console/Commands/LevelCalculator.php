<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Level;
use App\Repositories\LevelRepository;
use Illuminate\Console\Command;

class LevelCalculator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'level:calculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Requirements to level up';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->line('Level Calculator Start');

        // Check Level One
        $current_level = 1;
        $batches = [13, 11, 10, 9, 8, 7, 7];
        while ($current_level < 7) {
            $this->info('Level ' . $current_level);
            $this_level = Level::where('level', '>=', $current_level)->where('status', 1)->get();
            $legit_count = floor(count($this_level) / $batches[$current_level - 1]);
            $next_level = Level::where('level', $current_level + 1)->where('status', 1)->count();
            if ($next_level < $legit_count) {
                $to_level_up_count = $legit_count - $next_level;
                for ($i = 0; $i < $to_level_up_count; $i++) {
                    // Find the Legit to Level up
                    foreach ($this_level as $level) {
                        if ($this->_levelRule($level->account_id)) {
                            dump("{$level->account_id} is leveled up to {($level->level + 1)}");
                            (new LevelRepository())->levelUp($level);
                        }
                    }
                }
            }

            $current_level++;
        }

        $this->line('End');
    }

    private function _levelRule(int $account_id): bool
    {
        $l = Level::where('account_id', $account_id)->first();
        $level = $l->level;
        $count = $l->current_referrals;
        $amount = $l->current_purchases;
        switch ($level) {
            case 1:
                if ($count >= 2) return true;
                return false;
            case 2:
                if ($count >= 4) return true;
                return false;
            case 3:
                if ($count >= 8 && $amount >= 2000) return true;
                return false;
            case 4:
            case 5:
            case 6:
            case 7:
            default:
                return true;
        }
    }
}
