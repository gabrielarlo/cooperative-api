<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Level;
use App\Models\User;
use App\Repositories\AccountRepository;
use App\Repositories\AuthRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigrateMembers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'members:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migration of members to new DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        $members = DB::connection('mysql-secondary')->table('members')->get();
//        $count = count($members);
//        $this->line("Migrating {$count} records");
//
//        foreach ($members as $member) {
//            $name = $member->first_name;
//            if ($member->middle_name != null || strlen($member->middle_name) > 0) {
//                $name .= ' ' . $member->middle_name;
//            }
//            $name .= ' ' . $member->last_name;
//
//            $req = request();
//            $req['name'] = $name;
//            $req['email'] = $member->email;
//            $req['contact'] = $member->contact_number;
//            $req['gender'] = $member->gender;
//            $req['password'] = 'password@2021';
//            $req['password_confirmation'] = 'password@2021';
//
//            $register = (new AuthRepository())->register($req, true);
//            $register_code = $register->getData(true)['code'];
//            $register_data = $register->getData(true)['data'];
//            if ($register_code == 200) {
//                $account = $register_data['a'];
//                $user = $register_data['u'];
//
//                // For First Account encoded status
//                dump($account['id']);
//                $new_account = Account::find($account['id']);
//                $new_account->status = 1;
//                $new_account->encoded = (int)$member->is_encoded;
//                $new_account->save();
//
//                // Check if the first account has a referrer
//                if ($member->referrer_id > 0) {
//                    $old_referrer_account = DB::connection('mysql-secondary')->table('members')->find($member->referrer_id);
//                    $new_user = User::where('email', $old_referrer_account->email)->first();
//                    if ($new_user) {
//                        $new_account = Account::where('user_id', $new_user->id)->first();
//                        $first_account = Account::find($account['id']);
//                        $first_account->referrer_id = $new_account->id;
//                        $first_account->save();
//                    }
//                }
//
//                // Get Old Level
//                $old_level = DB::connection('mysql-secondary')->table('levels')->where('member_id', $member->id)->first();
//                if ($old_level) {
//                    $l = new Level;
//                    $l->level = $old_level->level;
//                    $l->account_id = $account['id'];
//                    $l->save();
//                }
//
//                $old_accounts = DB::connection('mysql-secondary')->table('members')->where('email', $member->email)->get();
//                if (count($old_accounts) > 1) {
//                    $new_account_id = $account['id'];
//                    $counter = 1;
//                    $pair = 1;
//                    foreach ($old_accounts as $old_account) {
//                        // For succeeding accounts
//                        if ($counter > 1) {
//                            $new_account = (new AccountRepository())->create($user['id'], $user['name'], $new_account_id, 1);
//                            if ($pair >= 2) {
//                                $new_account_id++;
//                                $pair = 1;
//                            } else {
//                                $pair++;
//                            }
//
//                            // Get Old Level
//                            $old_level = DB::connection('mysql-secondary')->table('levels')->where('member_id', $old_account->id)->first();
//                            if ($old_level) {
//                                $l = new Level;
//                                $l->level = $old_level->level;
//                                $l->account_id = $new_account->id;
//                                $l->save();
//                            }
//                        }
//                        $counter++;
//                    }
//                }
//            }
//        }
//        $this->line('End');
    }
}
