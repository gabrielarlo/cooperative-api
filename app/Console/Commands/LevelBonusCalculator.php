<?php

namespace App\Console\Commands;

use App\Abstracts\LedgerFlowType;
use App\Abstracts\Settings;
use App\Models\Account;
use App\Models\Ledger;
use App\Models\Level;
use App\Models\User;
use App\Repositories\LedgerRepository;
use Illuminate\Console\Command;

class LevelBonusCalculator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'level:bonus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check and Fill the level Bonus of each account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        $this->line('Level Bonus Start');
//
//        $current_level = 2;
//        while ($current_level <= 4) {
//            $l = Level::where('level', $current_level)->where('status', 1)->get();
//            foreach ($l as $level) {
//                $a = Account::find($level->account_id);
//                $l_count = Ledger::where('user_id', $a->user_id)->where('remarks', 'For leveling up to ' . $current_level)->count();
//                if ($l_count == 0) {
//                    $this->info('Filling for user ' . $a->user_id);
//                    $bonus = 0;
//                    if ($current_level == 2) {
//                        $bonus = Settings::LEVEL_TWO_BONUS;
//                    } elseif ($current_level == 3) {
//                        $bonus = Settings::LEVEL_THREE_BONUS;
//                    } elseif ($current_level == 4) {
//                        $bonus = Settings::LEVEL_FOUR_BONUS;
//                    }
//
//                    $u = User::find($a->user_id);
//                    (new LedgerRepository())->saveEntry($u, LedgerFlowType::LEVEL_BONUS, $bonus, 'For leveling up to ' . $current_level, 1);
//                }
//            }
//            $current_level++;
//        }
//
//        $this->line('End');
    }
}
