<?php

use App\Abstracts\ImageType;
use App\Models\User;
use Hashids\Hashids;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

function res($data = null, $msg = 'Success', $code = 200): JsonResponse
{
    return response()->json([
        'code' => $code,
        'msg' => $msg,
        'data' => $data,
    ]);
}

function vRes(Validator $validator): JsonResponse
{
    return res($validator->errors(), 'Validation Failed',  412);
}

function eRes($msg = '', $code = 400): JsonResponse
{
    return res(null, $msg, $code);
}

function encode($id, $connection = 'main'): string
{
    $config = config('hashids.connections.' . $connection);
    return (new Hashids($config['salt'], $config['length'], $config['alphabet']))->encode($id);
}

function decode($hash, $connection = 'main'): ?int
{
    $config = config('hashids.connections.' . $connection);
    $decode = (new Hashids($config['salt'], $config['length'], $config['alphabet']))->decode($hash);
    if (count($decode) > 0) return $decode[0];
    return null;
}

function saveImage(string $base64, string $imageType, int $id): string
{
    $name = $imageType . '-' . hash('sha256', $id);
    $path = $imageType;
    if ($imageType == ImageType::PRODUCT) {
        $name = $imageType . '-' . hash('sha256', $id + now()->timestamp);
        $path = $imageType . '/' . encode($id, 'uuid');
    }
    if (!File::exists($path)) File::makeDirectory($path, 0755, true);
    Image::make($base64)->save("{$path}/" . $name . '.jpg', 100, 'jpg');
    return $name . '.jpg';
}

function getImageUrl($imageType, $id, bool $thumbnail = true): ?string
{
    $image = null;
    if (File::exists(public_path("{$imageType}/{$imageType}-" .  hash('sha256', $id) . '.jpg'))) {
        $image =  url("/images/original/{$imageType}-" . hash('sha256', $id) . '.jpg');
        if ($imageType == 'avatar') {
            $u = User::find($id);
            if ($u) {
                $image = $image . '?date=' . now()->timestamp;
            }
        } elseif ($imageType == 'cover') {
            $image = $image . '?date=' . now()->timestamp;
        }
        if ($thumbnail) {
            $image = url("/images/thumbnail/{$imageType}-" . hash('sha256', $id) . '.jpg');
        }
    }
    return $image;
}

function getBannerUrls(): array
{
    $list = [];
    if (File::exists(public_path('banners'))) {
        $files = File::files(public_path('banners/'));
        foreach ($files as $file) {
            array_push($list, url('banners/' . $file->getFilename()));
        }
    }
    return $list;
}

function getProductImageUrls(int $id): array
{
    $list = [];
    $hashid = encode($id, 'uuid');
    if (File::exists(public_path('product/' . $hashid))) {
        $files = File::files(public_path('product/' . $hashid));
        foreach ($files as $file) {
            array_push($list, $file->getFilename());
        }
    }
    return $list;
}

function isAdmin(): bool
{
    $id = Auth::id();
    $u = User::find($id);
    if ($u->role == 9) return true;
    return false;
}

function pager($req): array
{
    $page = $req->page ?? 1;
    $limit = $req->limit ?? 10;
    $offset = ($page - 1) * $limit;
    return [
        'offset' => $offset,
        'limit' => $limit,
        'page' => $page,
    ];
}
