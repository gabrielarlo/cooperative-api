<?php


namespace App\Abstracts;


abstract class AddressSector
{
    const LUZON = 'luzon';
    const VISAYAS = 'visayas';
    const MINDANAO = 'mindanao';
}
