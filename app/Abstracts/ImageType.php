<?php


namespace App\Abstracts;


abstract class ImageType
{
    const AVATAR = 'avatar';
    const COVER = 'cover';
    const ATTACHMENT = 'attachment';
    const PRODUCT = 'product';
    const RECEIPT = 'receipt';
    const ANNOUNCEMENT = 'announcement';
}
