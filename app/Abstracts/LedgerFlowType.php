<?php


namespace App\Abstracts;


abstract class LedgerFlowType
{
    const REFERRAL_BONUS = 1;
    const TASK_COMPLETION = 2;
    const LEVEL_BONUS = 3;
    const PAYIN = 4;
    const WITHDRAWAL = 5;
    const TASK_CREATION = 6;
    const REVOKED_TASK = 7;

    const PURCHASE = 8;
    const CANCELLED_PURCHASE = 9;

    const TASK_REIMBURSEMENT = 10;
}
