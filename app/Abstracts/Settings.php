<?php


namespace App\Abstracts;


abstract class Settings
{
    const REFERRAL_BONUS = 250.00;
    const LEVEL_TWO_BONUS = 5000.00;
    const LEVEL_THREE_BONUS = 10000.00;
    const LEVEL_FOUR_BONUS = 20000.00;

    const PER_ACCOUNT = 2600.00;

    const TASK_CHARGE_PERCENTAGE = 0.03; // 3%
}
