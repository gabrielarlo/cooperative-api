<?php


namespace App\Abstracts;


abstract class TaskStatus
{
    const DELETED = 0;
    const RUNNING = 1;
    const COMPLETED = 2;
}
