<?php


namespace App\Abstracts;


abstract class OrderStatus
{
    const CANCELLED = 0;
    const PENDING = 1;
    const TODELIVER = 2;
    const ONDELIVERY = 3;
    const DELIVERED = 4;
}
