<?php


namespace App\Abstracts;


abstract class LedgerStatus
{
    const PENDING = 0;
    const COMPLETE = 1;
    const CANCELLED = 2;
}
