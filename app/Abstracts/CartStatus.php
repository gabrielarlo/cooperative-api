<?php


namespace App\Abstracts;


abstract class CartStatus
{
    const PENDING = 0;
    const CHECKOUT = 1;
    const ORDERED = 2;
    const REMOVED = 3;
}
