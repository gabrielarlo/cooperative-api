<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\AnnouncementInterface;
use Illuminate\Http\Request;

class AnnouncementController extends Controller
{
    protected $ann;

    public function __construct(AnnouncementInterface $ann)
    {
        $this->ann = $ann;
    }

    public function list()
    {
        return $this->ann->list(\request());
    }

    public function add()
    {
        return $this->ann->add(\request());
    }

    public function delete()
    {
        return $this->ann->delete(\request());
    }
}
