<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\AddressInterface;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    protected $address;

    public function __construct(AddressInterface $address)
    {
        $this->address = $address;
    }

    public function list()
    {
        return $this->address->list();
    }

    public function add()
    {
        return $this->address->add(\request());
    }

    public function update()
    {
        return $this->address->update(\request());
    }

    public function delete()
    {
        return $this->address->delete(\request());
    }

    public function makeDefault()
    {
        return $this->address->makeDefault(\request());
    }

    public function getDefault()
    {
        return $this->address->getDefault();
    }
}
