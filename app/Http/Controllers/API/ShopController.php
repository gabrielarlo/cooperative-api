<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\ShopInterface;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    protected $shop;

    public function __construct(ShopInterface $shop)
    {
        $this->shop = $shop;
    }

    public function saleList()
    {
        return $this->shop->saleList(\request());
    }

    public function shopList()
    {
        return $this->shop->shopList(\request());
    }

    public function list()
    {
        return $this->shop->list(\request());
    }

    public function add()
    {
        return $this->shop->add(\request());
    }

    public function update()
    {
        return $this->shop->update(\request());
    }

    public function getImages()
    {
        return $this->shop->getImages(\request());
    }

    public function uploadImage()
    {
        return $this->shop->uploadImage(\request());
    }

    public function deleteImage()
    {
        return $this->shop->deleteImage(\request());
    }

    public function cartList()
    {
        return $this->shop->cartList(\request());
    }

    public function addToCart()
    {
        return $this->shop->addToCart(\request());
    }

    public function subFromCart()
    {
        return $this->shop->subFromCart(\request());
    }

    public function removeFromCart()
    {
        return $this->shop->removeFromCart(\request());
    }

    public function addToCheckout()
    {
        return $this->shop->addToCheckout(\request());
    }

    public function order()
    {
        return $this->shop->order(\request());
    }

    public function validatePayment()
    {
        return $this->shop->validatePayment(\request());
    }

    public function getDeliveryCharges()
    {
        return $this->shop->getDeliveryCharges(\request());
    }

    public function getCategories()
    {
        return $this->shop->getCategories();
    }

    public function details()
    {
        return $this->shop->details(\request());
    }

    public function toggleStockStatus()
    {
        return $this->shop->toggleStockStatus(\request());
    }

    public function getBanners()
    {
        return $this->shop->getBanners();
    }

    public function getReviews()
    {
        return $this->shop->getReviews(\request());
    }
}
