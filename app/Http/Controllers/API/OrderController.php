<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\OrderInterface;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected $order;

    public function __construct(OrderInterface $order)
    {
        $this->order = $order;
    }

    public function list()
    {
        return $this->order->list(\request());
    }

    public function myList()
    {
        return $this->order->myList(\request());
    }

    public function updateStatus()
    {
        return $this->order->updateStatus(\request());
    }

    public function productList()
    {
        return $this->order->productList(\request());
    }

    public function productReview()
    {
        return $this->order->productReview(\request());
    }
}
