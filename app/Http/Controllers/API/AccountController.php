<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\AccountInterface;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    protected $account;

    public function __construct(AccountInterface $account)
    {
        $this->account = $account;
    }

    public function getStatus()
    {
        return $this->account->getStatus();
    }

    public function getReferralCodes()
    {
        return $this->account->getReferralCodes();
    }

    public function users()
    {
        return $this->account->users(\request());
    }

    public function changeActiveStatus()
    {
        return $this->account->changeActiveStatus(\request());
    }

    public function changeBanStatus()
    {
        return $this->account->changeBanStatus(\request());
    }
}
