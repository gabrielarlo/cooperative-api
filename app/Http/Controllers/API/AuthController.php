<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\AuthInterface;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $auth;

    public function __construct(AuthInterface $auth)
    {
        $this->auth = $auth;
    }

    public function register()
    {
        return $this->auth->register(\request());
    }

    public function login()
    {
        return $this->auth->login(\request());
    }

    public function verifyToken()
    {
        return $this->auth->verifyToken(\request());
    }

    public function requestPasswordReset()
    {
        return $this->auth->requestPasswordReset(\request());
    }

    public function resetPassword()
    {
        return $this->auth->resetPassword(\request());
    }

    public function update()
    {
        return $this->auth->update(\request());
    }

    public function changeAvatar()
    {
        return $this->auth->changeAvatar(\request());
    }

    public function changeCover()
    {
        return $this->auth->changeCover(\request());
    }

    public function changePassword()
    {
        return $this->auth->changePassword(\request());
    }

    public function activateAccount()
    {
        return $this->auth->activateAccount(\request());
    }

    public function toggleBan()
    {
        return $this->auth->toggleBan(\request());
    }

    public function logout()
    {
        return $this->auth->logout(\request());
    }

    public function checkReferralCode()
    {
        return $this->auth->checkReferralCode(\request());
    }
}
