<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\LedgerInterface;
use Illuminate\Http\Request;

class LedgerController extends Controller
{
    protected $ledger;

    public function __construct(LedgerInterface $ledger)
    {
        $this->ledger = $ledger;
    }

    public function mySummary()
    {
        return $this->ledger->mySummary();
    }

    public function summary()
    {
        return $this->ledger->summary();
    }

    public function list()
    {
        return $this->ledger->list(\request());
    }

    public function general()
    {
        return $this->ledger->general(\request());
    }
}
