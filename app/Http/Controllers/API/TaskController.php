<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\TaskInterface;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    protected $task;

    public function __construct(TaskInterface $task)
    {
        $this->task = $task;
    }

    public function list()
    {
        return $this->task->list(\request());
    }

    public function myList()
    {
        return $this->task->myList(\request());
    }

    public function allList()
    {
        return $this->task->allList(\request());
    }

    public function details()
    {
        return $this->task->details(\request());
    }

    public function create()
    {
        return $this->task->create(\request());
    }

    public function delete()
    {
        return $this->task->delete(\request());
    }

    public function end()
    {
        return $this->task->end(\request());
    }

    public function complete()
    {
        return $this->task->complete(\request());
    }

    public function reRun()
    {
        return $this->task->reRun(\request());
    }

    public function revoke()
    {
        return $this->task->revoke(\request());
    }
}
