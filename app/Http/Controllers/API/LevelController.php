<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\LevelInterface;
use Illuminate\Http\Request;

class LevelController extends Controller
{
    protected $level;

    public function __construct(LevelInterface $level)
    {
        $this->level = $level;
    }

    public function list()
    {
        return $this->level->list(\request());
    }
}
