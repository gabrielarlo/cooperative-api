<?php

namespace App\Http\Resources;

use App\Abstracts\ImageType;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

/**
 * @property mixed name
 * @property mixed email
 * @property mixed contact
 * @property mixed gender
 * @property mixed id
 * @property mixed ban
 * @property mixed status
 * @property mixed created_at
 * @property mixed updated_at
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $role = 'member';
        switch ($this->role) {
            case 9:
                $role = 'admin';
                break;
        }

        $avatar = getImageUrl(ImageType::AVATAR, $this->id);
        $cover = getImageUrl(ImageType::COVER, $this->id, false);

        return [
            'hashid' => encode($this->id, 'uuid'),
            'name' => $this->name,
            'email' => $this->email,
            'contact' => $this->contact,
            'gender' => $this->gender,
            'avatar' => $avatar ,
            'cover' => $cover,
            'role' => $role,
            'ban' => $this->ban == 1,
            'active' => $this->status == 1,
            'created_at' => $this->created_at->toString(),
            'updated_at' => $this->updated_at->toString(),
        ];
    }
}
