<?php

namespace App\Http\Resources;

use App\Abstracts\ImageType;
use App\Abstracts\OrderStatus;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $a = new AddressResource($this->address);
        $add = json_decode($a->toJson(), true);

        $status = '';
        switch ($this->status) {
            case OrderStatus::CANCELLED:
                $status = 'cancelled';
                break;
            case OrderStatus::PENDING:
                $status = 'pending';
                break;
            case OrderStatus::TODELIVER:
                $status = 'to-deliver';
                break;
            case OrderStatus::ONDELIVERY:
                $status = 'on-delivery';
                break;
            case OrderStatus::DELIVERED:
                $status = 'delivered';
                break;
        }
        return [
            'hashid' => encode($this->id, 'uuid'),
            'tracking' => $this->tracking,
            'buyer' => $this->user->name,
            'address' => $add['full'],
            'contact' => $add['contact'],
            'amount' => $this->amount,
            'delivery_charge' => $this->delivery_charge,
            'remarks' => $this->remarks,
            'for_delivery' => $this->for_delivery == 1,
            'method' => $this->method,
            'ref_no' => $this->ref_no,
            'image' => getImageUrl(ImageType::RECEIPT, $this->id, false),
            'status' => $status,
            'created_at' => $this->created_at->toString(),
            'my_order' => $this->user_id == Auth::id(),
        ];
    }
}
