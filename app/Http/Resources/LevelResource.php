<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LevelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'level' => $this->level,
            'name' => $this->account->name,
            'current_referrals' => $this->current_referrals,
            'current_spent' => $this->current_purchases,
            'referrals' => $this->account->referralCount(),
        ];
    }
}
