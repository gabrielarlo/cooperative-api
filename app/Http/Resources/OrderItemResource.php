<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid' => encode($this->id, 'uuid'),
            'product_hashid' => encode($this->product_id, 'uuid'),
            'product_name' => $this->product->name,
            'quantity' => $this->quantity,
            'amount' => $this->amount,
        ];
    }
}
