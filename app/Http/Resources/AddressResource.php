<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid' => encode($this->id, 'uuid'),
            'full' => "{$this->bldg} {$this->street}, {$this->barangay}, {$this->city}, {$this->sector}",
            'bldg' => $this->bldg,
            'street' => $this->street,
            'barangay' => $this->barangay,
            'city' => $this->city,
            'contact' => $this->contact,
            'sector' => $this->sector,
            'note' => $this->note,
            'default' => $this->default == 1,
            'status' => $this->status == 1,
        ];
    }
}
