<?php

namespace App\Http\Resources;

use App\Abstracts\ImageType;
use App\Models\Account;
use Illuminate\Http\Resources\Json\JsonResource;

class LeaderBoardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $a = Account::find($this->referrer_id);
        return [
            'avatar' => getImageUrl(ImageType::AVATAR, $a->user_id),
            'referrer' => $a->name,
            'aid' => $a->aid,
            'total' => $this->total,
        ];
    }
}
