<?php

namespace App\Http\Resources;

use App\Abstracts\ImageType;
use App\Models\Account;
use Illuminate\Http\Resources\Json\JsonResource;

class UserAccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $role = 'member';
        switch ($this->role) {
            case 9:
                $role = 'admin';
                break;
        }

        $avatar = getImageUrl(ImageType::AVATAR, $this->id);
        $cover = getImageUrl(ImageType::COVER, $this->id);

        $accounts = Account::with('level')->where('user_id', $this->id)->where('status', 1)->get()->pluck('level.level');
        $referrer = Account::where('user_id', $this->id)->orderBy('referrer_id')->first();

        $referrer_name = null;
        if ($referrer && $referrer->referrer) {
            $referrer_name = $referrer->referrer->name;
        }

        return [
            'hashid' => encode($this->id, 'uuid'),
            'name' => $this->name,
            'email' => $this->email,
            'contact' => $this->contact,
            'gender' => $this->gender,
            'avatar' => $avatar ,
            'cover' => $cover,
            'role' => $role,
            'ban' => $this->ban == 1,
            'active' => $this->status == 1,
            'created_at' => $this->created_at->toString(),
            'accounts' => $accounts,
            'referrer' => $referrer_name,
        ];
    }
}
