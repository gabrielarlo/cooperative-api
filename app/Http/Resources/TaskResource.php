<?php

namespace App\Http\Resources;

use App\Abstracts\ImageType;
use App\Abstracts\TaskStatus;
use App\Models\Task;
use App\Models\TaskCompletion;
use App\Repositories\TaskRepository;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $t = Task::find($this->id);
        $int_status = (new TaskRepository())->getStatus($t);
        $status = 'running';
        if ($int_status == TaskStatus::COMPLETED) {
            $status = 'completed';
        } elseif ($int_status == TaskStatus::DELETED) {
            $status = 'deleted';
        }

        $approved = TaskCompletion::where('task_id', $this->id)->where('status', 1)->count();

        return [
            'hashid' => encode($this->id, 'uuid'),
            'creator_name' => $this->creator->name,
            'creator_avatar' => getImageUrl(ImageType::AVATAR, $this->creator->id),
            'category' => $this->category,
            'title' => $this->title,
            'description' => $this->description,
            'link' => $this->link,
            'has_attachment' => $this->has_attachment == 1,
            'has_expiration' => $this->expiration_date != null,
            'expiration_date' => $this->expiration_date,
            'slots' => $this->slots,
            'reward' => $this->reward,
            'available' => $this->slots - $approved,
            'status' => $status,
            'created_at' => $this->created_at,
        ];
    }
}
