<?php

namespace App\Http\Resources;

use App\Abstracts\LedgerFlowType;
use App\Abstracts\LedgerStatus;
use Illuminate\Http\Resources\Json\JsonResource;

class LedgerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $type = '';
        switch ($this->type) {
            case LedgerFlowType::REFERRAL_BONUS:
                $type = 'referral bonus';
                break;
            case LedgerFlowType::TASK_COMPLETION:
                $type = 'task completion';
                break;
            case LedgerFlowType::LEVEL_BONUS:
                $type = 'level bonus';
                break;
            case LedgerFlowType::PAYIN:
                $type = 'payin';
                break;
            case LedgerFlowType::WITHDRAWAL:
                $type = 'withdrawal';
                break;
            case LedgerFlowType::TASK_CREATION:
                $type = 'task creation';
                break;
            case LedgerFlowType::REVOKED_TASK:
                $type = 'revoked task';
                break;
            case LedgerFlowType::PURCHASE:
                $type = 'purchased';
                break;
            case LedgerFlowType::CANCELLED_PURCHASE:
                $type = 'cancelled purchased';
                break;
            case LedgerFlowType::TASK_REIMBURSEMENT:
                $type = 'task reimbursement';
                break;
        }

        $status = 'pending';
        if ($this->status == LedgerStatus::COMPLETE) {
            $status = 'complete';
        } elseif ($this->status == LedgerStatus::CANCELLED) {
            $status = 'cancelled';
        }

        return [
            'hashid' => encode($this->id, 'uuid'),
            'user' => $this->user->name,
            'flow' => $this->flow == 1 ? 'credit' : 'debit',
            'type' => $type,
            'amount' => $this->amount,
            'remarks' => $this->remarks,
            'status' => $status,
            'created_at' => $this->created_at->toString(),
        ];
    }
}
