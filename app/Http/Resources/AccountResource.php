<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'aid' => $this->aid,
            'name' => $this->name,
            'referrer' => $this->referrer->name,
            'encoded' => $this->encoded == 1,
            'aim_id' => $this->aim_id,
            'aim_pin' => $this->aim_sec_pin,
            'remarks' => $this->remarks,
            'active' => $this->status == 1,
        ];
    }
}
