<?php

namespace App\Http\Resources;

use App\Abstracts\CartStatus;
use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $status = 'pending';
        if ($this->status == CartStatus::CHECKOUT) {
            $status = 'checkout';
        } elseif ($this->status == CartStatus::ORDERED) {
            $status = 'ordered';
        } elseif ($this->status == CartStatus::REMOVED) {
            $status = 'removed';
        }
        return [
            'hashid' => encode($this->id, 'uuid'),
            'product' => new ProductResource($this->product),
            'quantity' => $this->quantity,
            'status' => $status,
        ];
    }
}
