<?php

namespace App\Http\Resources;

use App\Abstracts\ImageType;
use Illuminate\Http\Resources\Json\JsonResource;

class AnnouncementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid' => encode($this->id, 'uuid'),
            'creator' => $this->creator->name,
            'title' => $this->title,
            'subject' => $this->subject,
            'content' => $this->content,
            'created_at' => $this->created_at->toString(),
            'image' => getImageUrl(ImageType::ANNOUNCEMENT, $this->id, false),
        ];
    }
}
