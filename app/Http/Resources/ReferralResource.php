<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReferralResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid' => encode($this->id, 'uuid'),
            'referral' => $this->referral->name,
            'email' => $this->referral->user->email,
            'contact' => $this->referral->user->contact,
            'amount' => $this->amount,
            'remarks' => $this->remarks,
            'created_at' => $this->created_at->toString(),
            'status' => $this->status == 1,
        ];
    }
}
