<?php

namespace App\Http\Resources;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->on_sale == 1) {
            $now = now();
            $exp_date = Carbon::parse($this->sale_expiration)->addDay();
            if ($now > $exp_date) {
                $p = Product::find($this->id);
                $p->on_sale = 0;
                $p->save();

                $this->on_sale = 0;
            }
        }

        $new = false;
        $created_at = Carbon::parse($this->created_at);
        if (now()->diffInDays($created_at) <= 3) {
            $new = true;
        }

        return [
            'hashid' => encode($this->id, 'uuid'),
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'price' => $this->price,
            'on_sale' => $this->on_sale == 1,
            'sale_price' => $this->sale_price,
            'sale_expiration' => $this->sale_expiration,
            'on_stock' => $this->on_stock == 1,
            'active' => $this->status == 1,
            'images' => getProductImageUrls($this->id),
            'ratings' => $this->reviews()['rating'],
            'rate_count' => $this->reviews()['count'],
            'weight' => $this->weight,
            'new' => $new,
        ];
    }
}
