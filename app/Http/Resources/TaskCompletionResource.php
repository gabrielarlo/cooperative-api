<?php

namespace App\Http\Resources;

use App\Abstracts\ImageType;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskCompletionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid' => encode($this->id, 'uuid'),
            'completer_name' => $this->completer->name,
            'completer_avatar' => getImageUrl(ImageType::AVATAR, $this->completer->id),
            'comment' => $this->comment,
            'attachment' => getImageUrl(ImageType::ATTACHMENT, $this->id),
            'status' => $this->status == 1 ? 'approved' : 'revoked',
        ];
    }
}
