<?php

namespace App\Http\Middleware;

use App\Models\Visitor;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Firewall
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->header('App-Origin') == null) return eRes('You are not allowed here', 401);

        $origin = $request->header('App-Origin');
        if (!in_array($origin, config('firewall.origin'))) return eRes('You are not allowed here', 401);

        session(['origin' => $request->header('App-Origin')]);

        if (Auth::user()) {
            $date = now()->toDateString();
            $time = now()->toTimeString();
            $uid = Auth::id();
            $v = Visitor::where('user_id', $uid)->where('date', $date)->first();
            if (!$v) {
                $v = new Visitor;
                $v->user_id = $uid;
                $v->date = $date;
                $v->time = $time;
                $v->save();
            } else {
                $v->time = $time;
                $v->save();
            }
        }

        return $next($request);
    }
}
