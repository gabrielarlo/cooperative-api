<?php


namespace App\Repositories;


use App\Abstracts\ImageType;
use App\Abstracts\LedgerFlowType;
use App\Abstracts\Settings;
use App\Http\Resources\AccountResource;
use App\Http\Resources\UserResource;
use App\Interfaces\AuthInterface;
use App\Mail\EmailVerification;
use App\Models\Account;
use App\Models\Level;
use App\Models\OauthAccessToken;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AuthRepository implements AuthInterface
{

    public function register($req, bool $internal = false): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'contact' => 'required',
            'gender' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);
        if ($v->fails()) return vRes($v);

        $verification_token = encode(now()->timestamp, 'token');
        $same_verification_token = User::where('verification_token', $verification_token)->count();
        $has = true;
        while ($has) {
            if ($same_verification_token > 0) {
                $verification_token = encode(now()->timestamp + rand(), 'token');
                $same_verification_token = User::where('verification_token', $verification_token)->count();
            } else {
                $has = false;
            }
        }

        $u = new User;
        $u->name = $req->name;
        $u->email = $req->email;
        $u->verification_token = $verification_token;
        $u->contact = $req->contact;
        $u->gender = $req->gender;
        $u->password = bcrypt($req->password);
        $u->ban = 0;
        $u->status = 0;
        $u->save();

        $a = (new AccountRepository())->create($u->id, $req->name, 0, 0, 'Main Account');

        if ($req->has('referral_code') && strlen($req->referral_code) > 0) {
            $referrer = Account::where('aid', $req->referral_code)->where('status', 1)->first();
            if ($referrer) {
                $a->referrer_id = $referrer->id;
                $a->save();

                $l = Level::where('account_id', $referrer->id)->first();
                $l->current_referrals = $l->current_referrals + 1;
                $l->save();

                $result = (new ReferralRepository())->saveNewReferral($referrer, $a);

                if ($result) {
                    (new LedgerRepository())->saveEntry($referrer->user, LedgerFlowType::REFERRAL_BONUS, Settings::REFERRAL_BONUS, 'New referral');
                }
            } else {
                return eRes('Invalid Referral Code');
            }
        }

        Mail::to($u)->send(new EmailVerification($u));

        if ($internal) {
            return res(compact('u', 'a'));
        }

        return res();
    }

    public function login($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $u = User::where('email', $req->email)->where('ban', 0)->where('status', 1)->first();
        if (!$u) return eRes('Invalid Email');

        if (!Hash::check($req->password, $u->password)) return eRes('Invalid Password');

        $data = $this->_generateLoginData($u);

        return res($data);
    }

    public function verifyToken($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'token' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $u = User::where('verification_token', $req->token)->first();
        if (!$u) return eRes('Invalid Token');

        if ($u->ban == 0 && $u->status == 0) {
            $u->status = 1;
            $u->verification_token = null;
            $u->save();
        }

        return res();
    }

    public function requestPasswordReset($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'email' => 'required|email',
        ]);
        if ($v->fails()) return vRes($v);

        $u = User::where('email', $req->email)->where('ban', 0)->where('status', 1)->first();
        if (!$u) return eRes('Invalid Email');

        $u->verification_token = encode(now()->timestamp, 'token');
        $u->save();

        Mail::to($u)->send(new EmailVerification($u));

        return res();
    }

    public function resetPassword($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'token' => 'required',
            'password' => 'required|confirmed|min:6'
        ]);
        if ($v->fails()) return vRes($v);

        $u = User::where('verification_token', $req->token)->first();
        if (!$u) return eRes('Invalid Token');

        $u->password = bcrypt($req->password);
        $u->verification_token = null;
        $u->save();

        return res();
    }

    public function update($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'contact' => 'required',
            'gender' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = Auth::id();
        $ou = User::where('id', '!=', $id)->where('email', $req->email)->first();
        if ($ou) return eRes('Email Address is already taken');

        $u = User::find($id);
        $u->name = $req->name;
        $u->email = $req->email;
        $u->contact = $req->contact;
        $u->gender = $req->gender;
        $u->save();

        $data = new UserResource($u);

        return res($data);
    }

    public function changeAvatar($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'image_base_64' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        saveImage($req->image_base_64, ImageType::AVATAR, Auth::id());

        $u = User::find(Auth::id());
        $data = new UserResource($u);

        return res($data);
    }

    public function changeCover($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'image_base_64' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        saveImage($req->image_base_64, ImageType::COVER, Auth::id());

        $u = User::find(Auth::id());
        $data = new UserResource($u);

        return res($data);
    }

    public function changePassword($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'current_password' => 'required',
            'password' => 'required|confirmed|min:6'
        ]);
        if ($v->fails()) return vRes($v);

        $u = User::find(Auth::id());

        if (!Hash::check($req->current_password, $u->password)) return eRes('Invalid current password');

        $u->password = bcrypt($req->password);
        $u->save();

        return res();
    }

    public function logout($req): JsonResponse
    {
        $id = Auth::id();
        $origin = session('origin');
        OauthAccessToken::where('user_id', $id)->where('name', $origin)->delete();

        return res();
    }

    public function activateAccount($req): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid Permission');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $uid = decode($req->hashid, 'uuid');
        $u = User::find($uid);
        if (!$u) return eRes('User not found');

        $u->status = 1;
        $u->save();

        return res();
    }

    public function toggleBan($req): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid Permission');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $uid = decode($req->hashid, 'uuid');
        $u = User::find($uid);
        if (!$u) return eRes('User not found');

        $u->ban = $u->ban == 1 ? 0 : 1;
        $u->save();

        return res();
    }

    private function _generateLoginData(User $user): array
    {
        $origin = session('origin');
        OauthAccessToken::where('user_id', $user->id)->where('name', $origin)->delete();

        return [
            'token' => $user->createToken($origin)->accessToken,
            'user' => new UserResource($user),
        ];
    }

    public function checkReferralCode($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'referral_code' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $a = Account::where('aid', $req->referral_code)->where('status', 1)->first();
        if (!$a) return eRes('Invalid referral code');

        $data = new AccountResource($a);

        return res($data);
    }
}
