<?php


namespace App\Repositories;


use App\Abstracts\LedgerFlowType;
use App\Abstracts\LedgerStatus;
use App\Abstracts\OrderStatus;
use App\Http\Resources\LedgerResource;
use App\Interfaces\LedgerInterface;
use App\Models\Account;
use App\Models\Ledger;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class LedgerRepository implements LedgerInterface
{

    public function saveEntry(User $user, int $type, $amount = 0, string $remarks = '', int $status = 0): Ledger
    {
        $flow = 1;
        if ($type == LedgerFlowType::WITHDRAWAL || $type == LedgerFlowType::TASK_CREATION || $type == LedgerFlowType::REVOKED_TASK) {
            $flow = 0;
        }
        $l = new Ledger;
        $l->user_id = $user->id;
        $l->flow = $flow;
        $l->type = $type;
        $l->amount = $amount;
        $l->remarks = $remarks;
        $l->status = $status;
        $l->save();

        return $l;
    }

    public function mySummary(): JsonResponse
    {
        $id = Auth::id();
        $flowIn = Ledger::where('user_id', $id)->where('status', 1)->where('type', '!=', LedgerFlowType::PURCHASE)->where('flow', 1)->sum('amount');
        $flowOut = Ledger::where('user_id', $id)->where('status', 1)->where('flow', 0)->sum('amount');
        $available = $flowIn - $flowOut;
        $total_earnings = $flowIn;
        $referrals = Ledger::where('user_id', $id)->where('status', 1)->where('type', LedgerFlowType::REFERRAL_BONUS)->sum('amount');
        $task_rewards = Ledger::where('user_id', $id)->where('status', 1)->where('type', LedgerFlowType::TASK_COMPLETION)->sum('amount');
        $account = Account::where('user_id', $id)->where('status', 1)->count();

        return res(compact('available', 'total_earnings', 'referrals', 'task_rewards', 'account'));
    }

    public function summary(): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid Permission');

        $flowIn = Ledger::where('status', 1)->where('flow', 1)->sum('amount');
        $flowOut = Ledger::where('status', 1)->where('flow', 0)->sum('amount');
        $available = $flowIn - $flowOut;
        $total_earnings = $flowIn;
        $referrals = Ledger::where('status', 1)->where('type', LedgerFlowType::REFERRAL_BONUS)->sum('amount');
        $task_rewards = Ledger::where('status', 1)->where('type', LedgerFlowType::TASK_COMPLETION)->sum('amount');
        $account = Account::where('status', 1)->count();

        $sales = Ledger::where('status', 1)->where('type', LedgerFlowType::PURCHASE)->sum('amount') - Ledger::where('status', 1)->where('type', LedgerFlowType::CANCELLED_PURCHASE)->sum('amount');
        $tasks = 0;
        $pending_orders = Order::where('status', OrderStatus::PENDING)->count();
        $total_orders = Order::count();

        return res(compact('available', 'total_earnings', 'referrals', 'task_rewards', 'account', 'sales', 'tasks', 'pending_orders', 'total_orders'));
    }

    public function list($req): JsonResponse
    {
        $pager = pager($req);

        $count = Ledger::where('user_id', Auth::id())->count();
        $l = Ledger::where('user_id', Auth::id())->where('type', '!=', LedgerFlowType::PURCHASE)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        $page = $pager['page'];

        $list = LedgerResource::collection($l);
        return res(compact('list', 'count', 'page'));
    }

    public function general($req): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid permission');

        $pager = pager($req);
        $count = Ledger::count();
        $l = Ledger::orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        $page = $pager['page'];

        $list = LedgerResource::collection($l);
        return res(compact('list', 'count', 'page'));
    }

    public function changeStatus(Ledger $ledger, int $status = LedgerStatus::COMPLETE): bool
    {
        if ($ledger == null) return false;

        $l = Ledger::find($ledger->id);
        $l->status = $status;
        $l->save();

        return true;
    }
}
