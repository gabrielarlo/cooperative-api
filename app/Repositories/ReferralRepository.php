<?php


namespace App\Repositories;


use App\Abstracts\Settings;
use App\Http\Resources\LeaderBoardResource;
use App\Http\Resources\ReferralResource;
use App\Interfaces\ReferralInterface;
use App\Models\Account;
use App\Models\Referral;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReferralRepository implements ReferralInterface
{

    public function saveNewReferral(Account $referrer, Account $referral, string $remarks = 'Direct Referral', int $status = 0): bool
    {
        $old = Referral::where('referrer_id', $referrer->id)->where('referral_id', $referral->id)->count();
        if ($old > 0) return false;

        $r = new Referral;
        $r->referrer_id = $referrer->id;
        $r->referral_id = $referral->id;
        $r->amount = Settings::REFERRAL_BONUS;
        $r->remarks = $remarks;
        $r->status = $status;
        $r->save();
        return true;
    }

    public function list(?int $user_id): JsonResponse
    {
        $user_id = $user_id ?? Auth::id();
        $account_ids = Account::where('user_id', $user_id)->pluck('id')->toArray();
        $r = Referral::whereIn('referrer_id', $account_ids)->get();
        $count = count($r);
        $page = 1;

        $list = ReferralResource::collection($r);
        return res(compact('list', 'count', 'page'));
    }

    public function topTen(): JsonResponse
    {
        $start = now()->startOfMonth();
        $end = now()->endOfMonth();
        $top = Referral::where('created_at', '>=', $start)->where('created_at', '<=', $end)->where('status', 1)->groupBy('referrer_id')->select('referrer_id', DB::raw('count(*) as total'))->orderBy('total', 'desc')->limit(10)->get();
        $data = LeaderBoardResource::collection($top);
        return res($data);
    }
}
