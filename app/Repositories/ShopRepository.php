<?php


namespace App\Repositories;


use App\Abstracts\AddressSector;
use App\Abstracts\CartStatus;
use App\Abstracts\ImageType;
use App\Abstracts\LedgerFlowType;
use App\Abstracts\OrderStatus;
use App\Http\Resources\CartResource;
use App\Http\Resources\ProductRatingResource;
use App\Http\Resources\ProductResource;
use App\Interfaces\ShopInterface;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\ProductRating;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ShopRepository implements ShopInterface
{
    public function saleList($req): JsonResponse
    {
        $search = $req->search ?? '';
        $pager = pager($req);

        $count = Product::where('status', 1)->where('on_sale', 1)->where('name', 'LIKE', "%{$search}%")->where('on_stock', 1)->count();
        $page = $pager['page'];
        $p = Product::where('status', 1)->where('on_sale', 1)->where('name', 'LIKE', "%{$search}%")->where('on_stock', 1)->orderBy('sale_expiration', 'asc')->offset($pager['offset'])->limit($pager['limit'])->get();
        $list = ProductResource::collection($p);

        return res(compact('count', 'page', 'list'));
    }

    public function shopList($req): JsonResponse
    {
        $category = $req->category ?? 'all';
        $search = $req->search ?? '';
        $pager = pager($req);

        if ($category == 'all') {
            $count = Product::where('status', 1)->where('on_stock', 1)->where('name', 'LIKE', "%{$search}%")->count();
            $page = $pager['page'];
            $p = Product::where('status', 1)->where('on_stock', 1)->where('name', 'LIKE', "%{$search}%")->orderBy('updated_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
            $list = ProductResource::collection($p);
        } elseif ($category == 'sale') {
            return $this->saleList($req);
        } else {
            $c = Category::where('name', $category)->where('status', 1)->first();
            if (!$c) return eRes('Category not found');

            $count = Product::where('status', 1)->where('on_stock', 1)->where('name', 'LIKE', "%{$search}%")->where('category_id', $c->id)->count();
            $page = $pager['page'];
            $p = Product::where('status', 1)->where('on_stock', 1)->where('name', 'LIKE', "%{$search}%")->where('category_id', $c->id)->orderBy('updated_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
            $list = ProductResource::collection($p);
        }

        return res(compact('count', 'page', 'list'));
    }

    public function list($req): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid permission');

        $pager = pager($req);

        $count = Product::count();
        $page = $pager['page'];
        $p = Product::orderBy('updated_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        $list = ProductResource::collection($p);

        return res(compact('count', 'page', 'list'));
    }

    public function add($req): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid Permission');

        $v = Validator::make($req->all(), [
            'name' => 'required',
            'description' => 'required|max:1000',
            'price' => 'required',
            'on_sale' => 'required|min:0|max:1',
            'weight' => 'required|min:1',
        ]);
        if ($v->fails()) return vRes($v);

        if ($req->on_sale == 1) {
            $v = Validator::make($req->all(), [
                'sale_price' => 'required',
            ]);
            if ($v->fails()) return vRes($v);
        }

        $slug = Str::slug($req->name);
        $p = Product::where('slug', $slug)->first();
        if ($p) return eRes('Name is already existed');

        $category = $req->category;
        if ($category == null || $category == '') {
            $category = 'general';
        }
        $category = strtolower($category);
        $c = Category::where('name', $category)->first();
        if (!$c) {
            $c = new Category;
            $c->name = $category;
            $c->save();
        }

        $p = new Product;
        $p->name = $req->name;
        $p->slug = $slug;
        $p->description = $req->description;
        $p->price = $req->price;
        $p->on_sale = $req->on_sale;
        $p->sale_price = $req->sale_price;
        $p->sale_expiration = $req->sale_expiration;
        $p->weight = $req->weight;
        $p->on_stock = 1;
        $p->status = 1;
        $p->category_id = $c->id;
        $p->save();

        $data = new ProductResource($p);

        return res($data);
    }

    public function update($req): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid Permission');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
            'name' => 'required',
            'description' => 'required|max:1000',
            'price' => 'required',
            'on_sale' => 'required|min:0|max:1',
            'weight' => 'required|min:1',
        ]);
        if ($v->fails()) return vRes($v);

        if ($req->on_sale == 1) {
            $v = Validator::make($req->all(), [
                'sale_price' => 'required',
            ]);
            if ($v->fails()) return vRes($v);
        }

        $id = decode($req->hashid, 'uuid');
        $slug = Str::slug($req->name);
        $p = Product::where('slug', $slug)->where('id', '!=', $id)->first();
        if ($p) return eRes('Name is already existed');

        $p = Product::find($id);
        if (!$p) return eRes('Product not found');

        $category = $req->category;
        if ($category == null || $category == '') {
            $category = 'general';
        }
        $category = strtolower($category);
        $c = Category::where('name', $category)->first();
        if (!$c) {
            $c = new Category;
            $c->name = $category;
            $c->save();
        }

        $p->name = $req->name;
        $p->slug = $slug;
        $p->description = $req->description;
        $p->price = $req->price;
        $p->on_sale = $req->on_sale;
        $p->sale_price = $req->sale_price;
        $p->sale_expiration = $req->sale_expiration;
        $p->weight = $req->weight;
        $p->category_id = $c->id;
        $p->save();

        $data = new ProductResource($p);

        return res($data);
    }

    public function getImages($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $p = Product::find($id);
        if (!$p) return eRes('Product not found');

        $images = getProductImageUrls($p->id);
        return res($images);
    }

    public function uploadImage($req): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid Permission');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
            'image_base_64' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $p = Product::find($id);
        if (!$p) return eRes('Product not found');

        saveImage($req->image_base_64, ImageType::PRODUCT, $p->id);

        $images = getProductImageUrls($p->id);
        return res($images);
    }

    public function deleteImage($req): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid Permission');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
            'image_name' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $p = Product::find($id);
        if (!$p) return eRes('Product not found');

        if (!File::exists(public_path('product/' . $req->hashid . '/' . $req->image_name))) return eRes('Image not found');

        File::delete(public_path('product/' . $req->hashid . '/' . $req->image_name));

        $images = getProductImageUrls($p->id);
        return res($images);
    }

    public function cartList($req): JsonResponse
    {
        $c = Cart::where('user_id', Auth::id())->whereNotIn('status', [CartStatus::ORDERED, CartStatus::REMOVED])->get();

        $data = CartResource::collection($c);
        return res($data);
    }

    public function addToCart($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'hashid' => 'required',
            'quantity' => 'required|min:1',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $p = Product::find($id);
        if (!$p) return eRes('Product not found');

        $c = Cart::where('user_id', Auth::id())->where('product_id', $p->id)->whereNotIn('status', [CartStatus::ORDERED, CartStatus::REMOVED])->first();
        if (!$c) {
            $c = new Cart;
        }

        $c->user_id = Auth::id();
        $c->product_id = $p->id;
        $c->quantity = ($c->quantity ?? 0) + $req->quantity;
        $c->status = $c->status ?? CartStatus::PENDING;
        $c->save();

        return res();
    }

    public function subFromCart($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'hashid' => 'required',
            'quantity' => 'required|min:1',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $p = Product::find($id);
        if (!$p) return eRes('Product not found');

        $c = Cart::where('user_id', Auth::id())->where('product_id', $p->id)->whereNotIn('status', [CartStatus::ORDERED, CartStatus::REMOVED])->first();
        if (!$c) {
            $c = new Cart;
        }

        $c->user_id = Auth::id();
        $c->product_id = $p->id;
        $c->quantity = ($c->quantity ?? 0) - $req->quantity;
        $c->status = $c->status ?? CartStatus::PENDING;
        $c->save();

        if ($c->quantity < 0) {
            try {
                $c->delete();
            } catch (\Exception $e) {
                return eRes($e->getMessage());
            }
        }

        return res();
    }

    public function removeFromCart($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $p = Product::find($id);
        if (!$p) return eRes('Product not found');

        $c = Cart::where('user_id', Auth::id())->where('product_id', $p->id)->whereNotIn('status', [CartStatus::ORDERED, CartStatus::REMOVED])->first();
        if (!$c) return eRes('Item not found');

        $c->status = CartStatus::REMOVED;
        $c->save();

        return res();
    }

    public function addToCheckout($req): JsonResponse
    {
        $c = Cart::where('user_id', Auth::id())->whereNotIn('status', [CartStatus::ORDERED, CartStatus::REMOVED])->get();
        if (count($c) == 0) return eRes('No cart items');

        foreach ($c as $cart) {
            $uc = Cart::find($cart->id);
            $uc->status = CartStatus::CHECKOUT;
            $uc->save();
        }

        return res();
    }

    public function order($req): JsonResponse
    {
        $c = Cart::where('user_id', Auth::id())->where('status', CartStatus::CHECKOUT)->get();
        if (count($c) == 0) return eRes('No cart items');

        $v = Validator::make($req->all(), [
            'method' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        if ($req->method != 'cod') {
            $v = Validator::make($req->all(), [
                'ref_no' => 'required',
                'image_base_64' => 'required',
            ]);
            if ($v->fails()) return vRes($v);
        }

        $u = User::find(Auth::id());
        $a = $u->currentAddress();
        if (!$a) return eRes('No default address');

        $o = new Order;
        $o->user_id = $u->id;
        $o->tracking = encode(now()->timestamp, 'tracking');
        $o->address_id = $a->id;
        $o->method = $req->method;
        $o->ref_no = $req->ref_no ?? '';
        $o->amount = 0; // To be fill below
        $o->delivery_charge = 0; // To be fill below
        $o->remarks = $req->remarks ?? '';
        $o->save();

        if ($req->method != 'cod') {
            saveImage($req->image_base_64, ImageType::RECEIPT, $o->id);
        }

        $amount = 0.0;
        $weight = 0.0;
        foreach ($c as $cart) {
            $uc = Cart::find($cart->id);
            $uc->status = CartStatus::ORDERED;
            $uc->save();

            $price = $cart->product->price;
            if ($cart->product->on_sale == 1) {
                $price = $cart->product->sale_price;
            }

            $amount += $price * $cart->quantity;
            $weight += $cart->product->weight;

            $oi = new OrderItem;
            $oi->product_id = $cart->product->id;
            $oi->order_id = $o->id;
            $oi->quantity = $cart->quantity;
            $oi->amount = $price * $cart->quantity;
            $oi->status = 1;
            $oi->save();
        }
        $o->amount = $amount;
        $o->delivery_charge = $this->_deliveryCharge($weight, $u->currentAddress()->sector);
        $o->for_delivery = OrderStatus::PENDING;
        $o->save();

        (new LedgerRepository())->saveEntry($u, LedgerFlowType::PURCHASE, $amount, 'Purchased ' . $o->tracking); // Can be revoked via cancel

        return res();
    }

    public function validatePayment($req)
    {
        // TODO: Implement validatePayment() method.
    }

    public function getDeliveryCharges($req): JsonResponse
    {
        $default = (new AddressRepository())->getDefault()->getData(true);
        if ($default['code'] != 200) return eRes($default['msg']);
        $sector = $default['data']['sector'];

        $data = $this->_deliveryCharge(1, $sector, true);
        return res($data);
    }

    private function _deliveryCharge($weight = 1, $sector = 'mindanao', $is_range = false)
    {
        $cWeight = ceil($weight);
        if ($cWeight < 1) {
            $cWeight = 1;
        }
        if ($cWeight > 3) {
            $cWeight = 3;
        }

        $table = [
            AddressSector::LUZON => [
                1 => 250.0,
                2 => 280.0,
                3 => 350.0,
            ],
            AddressSector::VISAYAS => [
                1 => 220.0,
                2 => 250.0,
                3 => 280.0,
            ],
            AddressSector::MINDANAO => [
                1 => 190.0,
                2 => 220.0,
                3 => 250.0,
            ],
        ];

        if ($is_range) return array_values($table[$sector]);
        return $table[$sector][$cWeight];
    }

    public function getCategories(): JsonResponse
    {
        $c = Category::where('status', 1)->pluck('name');

        return res($c);
    }

    public function details($req): JsonResponse
    {
        $slug = $req->slug ?? '';

        $p = Product::where('slug', $slug)->where('status', 1)->first();
        if (!$p) return eRes('Product not found');

        $data = new ProductResource($p);
        return res($data);
    }

    public function toggleStockStatus($req): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid permission');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $p = Product::find($id);
        if (!$p) return eRes('Product not found');

        $p->on_stock = $p->on_stock == 1 ? 0 : 1;
        $p->save();

        return res();
    }

    public function getBanners(): JsonResponse
    {
        $banners = getBannerUrls();
        return res($banners);
    }

    public function getReviews($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $p = Product::find($id);
        if (!$p) return eRes('Product not found');

        $pager = pager($req);

        $page = $pager['page'];
        $count = ProductRating::where('product_id', $id)->where('status', 1)->count();
        $pr = ProductRating::where('product_id', $id)->where('status', 1)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        $list = ProductRatingResource::collection($pr);

        return res(compact('page', 'count', 'list'));
    }
}
