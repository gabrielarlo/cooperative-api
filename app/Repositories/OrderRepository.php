<?php


namespace App\Repositories;


use App\Abstracts\LedgerFlowType;
use App\Abstracts\LedgerStatus;
use App\Abstracts\OrderStatus;
use App\Abstracts\Settings;
use App\Http\Resources\OrderItemResource;
use App\Http\Resources\OrderResource;
use App\Interfaces\OrderInterface;
use App\Models\Account;
use App\Models\Ledger;
use App\Models\Level;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\ProductRating;
use App\Models\Referral;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OrderRepository implements OrderInterface
{

    public function list($req): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid permission');

        $filter = $req->filter ?? 'all';
        $pager = pager($req);

        if ($filter == 'pending') {
            $count = Order::where('status', OrderStatus::PENDING)->count();
            $o = Order::where('status', OrderStatus::PENDING)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        } elseif ($filter == 'to-deliver') {
            $count = Order::where('status', OrderStatus::TODELIVER)->count();
            $o = Order::where('status', OrderStatus::TODELIVER)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        } elseif ($filter == 'on-delivery') {
            $count = Order::where('status', OrderStatus::ONDELIVERY)->count();
            $o = Order::where('status', OrderStatus::ONDELIVERY)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        } elseif ($filter == 'delivered') {
            $count = Order::where('status', OrderStatus::DELIVERED)->count();
            $o = Order::where('status', OrderStatus::DELIVERED)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        } elseif ($filter == 'cancelled') {
            $count = Order::where('status', OrderStatus::CANCELLED)->count();
            $o = Order::where('status', OrderStatus::CANCELLED)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        } else {
            $count = Order::count();
            $o = Order::orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        }
        $page = $pager['page'];
        $list = OrderResource::collection($o);

        return res(compact('count', 'page', 'list'));
    }

    public function myList($req): JsonResponse
    {
        $filter = $req->filter ?? 'all';
        $pager = pager($req);

        if ($filter == 'pending') {
            $count = Order::where('user_id', Auth::id())->where('status', OrderStatus::PENDING)->count();
            $o = Order::where('user_id', Auth::id())->where('status', OrderStatus::PENDING)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        } elseif ($filter == 'to-deliver') {
            $count = Order::where('user_id', Auth::id())->where('status', OrderStatus::TODELIVER)->count();
            $o = Order::where('user_id', Auth::id())->where('status', OrderStatus::TODELIVER)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        } elseif ($filter == 'on-delivery') {
            $count = Order::where('user_id', Auth::id())->where('status', OrderStatus::ONDELIVERY)->count();
            $o = Order::where('user_id', Auth::id())->where('status', OrderStatus::ONDELIVERY)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        } elseif ($filter == 'delivered') {
            $count = Order::where('user_id', Auth::id())->where('status', OrderStatus::DELIVERED)->count();
            $o = Order::where('user_id', Auth::id())->where('status', OrderStatus::DELIVERED)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        } elseif ($filter == 'cancelled') {
            $count = Order::where('user_id', Auth::id())->where('status', OrderStatus::CANCELLED)->count();
            $o = Order::where('user_id', Auth::id())->where('status', OrderStatus::CANCELLED)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        } else {
            $count = Order::where('user_id', Auth::id())->count();
            $o = Order::where('user_id', Auth::id())->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        }
        $page = $pager['page'];
        $list = OrderResource::collection($o);

        return res(compact('count', 'page', 'list'));
    }

    public function updateStatus($req): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid Permission');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
            'status' => 'required|min:0|max:4',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $o = Order::find($id);
        if (!$o) return eRes('Order not found');

        $o->status = $req->status;
        $o->save();

        if ($o->status == OrderStatus::DELIVERED) {
            $l = Ledger::where('remarks', 'LIKE', "%{$o->tracking}")->first();
            if ($l) {
                (new LedgerRepository())->changeStatus($l, LedgerStatus::COMPLETE);

                $u = User::find($o->user_id);
                if ($o->amount >= Settings::PER_ACCOUNT) {
                    // For first account
                    $acc = Account::where('user_id', $u->id)->where('status', 0)->first();
                    if ($acc) {
                        $acc->status = 1;
                        $acc->save();

                        $l = Level::where('account_id', $acc->id)->first();
                        $l->status = 1;
                        $l->save();

                        $r = Referral::where('referral_id', $acc->id)->first();
                        $r->status = 1;
                        $r->save();

                        (new LedgerRepository())->saveEntry($u, LedgerFlowType::REFERRAL_BONUS, Settings::REFERRAL_BONUS, 'Activating an account', 1);
                    } else {
                        // For recursive account
                        $count = floor($o->amount / Settings::PER_ACCOUNT);
                        for ($i = 0; $i < $count; $i++) {
                            $ref_id = 0;
                            $account_ids = Account::where('user_id', $u->id)->pluck('id');
                            $step = 2;
                            $index = 0;
                            while ($ref_id == 0) {
                                if (count($account_ids) <= $step) {
                                    $ref_id = $account_ids[$index];
                                }
                                $step += 2;
                                $index++;
                            }

                            $referral = (new AccountRepository())->create($u->id, $u->name, $ref_id, 1, 'For buying products');

                            $referrer = Account::find($ref_id);
                            $result = (new ReferralRepository())->saveNewReferral($referrer, $referral, 'Own Account by purchasing', 1);
                            if ($result) {
                                (new LedgerRepository())->saveEntry($referrer->user, LedgerFlowType::REFERRAL_BONUS, Settings::REFERRAL_BONUS, 'New Own referral', 1);
                            }
                        }
                        // Update Level MileStone
                        $account_ids = Account::where('user_id', $u->id)->pluck('id');
                        $l = Level::where('account_id', $account_ids[0])->first();
                        Level::whereIn('account_id', $account_ids)->update(['current_purchases' => $l->current_purchases + $o->amount]);
                    }
                }
            }
        } elseif ($o->status == OrderStatus::CANCELLED) {
            $l = Ledger::where('remarks', 'LIKE', "%{$o->tracking}")->first();
            if ($l) {
                (new LedgerRepository())->changeStatus($l, LedgerStatus::CANCELLED);
            }
        }

        $data = new OrderResource($o);
        return res($data);
    }

    public function productList($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $o = Order::find($id);
        if (!$o) return eRes('Order not found');

        $oi = OrderItem::where('order_id', $o->id)->where('status', 1)->get();

        $data = OrderItemResource::collection($oi);
        return res($data);
    }

    public function productReview($req)
    {
        $v = Validator::make($req->all(), [
            'hashid' => 'required',
            'rating' => 'required|min:1|max:5',
            'review' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $pid = decode($req->hashid, 'uuid');
        $p = Product::find($pid);
        if (!$p) return eRes('Product not found');

        $pr = new ProductRating;
        $pr->product_id = $p->id;
        $pr->user_id = Auth::id();
        $pr->rating = $req->rating;
        $pr->review = $req->review;
        $pr->save();

        return res();
    }
}
