<?php


namespace App\Repositories;


use App\Abstracts\ImageType;
use App\Abstracts\LedgerFlowType;
use App\Abstracts\Settings;
use App\Abstracts\TaskStatus;
use App\Http\Resources\TaskCompletionResource;
use App\Http\Resources\TaskResource;
use App\Interfaces\TaskInterface;
use App\Models\Task;
use App\Models\TaskCompletion;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TaskRepository implements TaskInterface
{

    public function list($req): JsonResponse
    {
        $pager = pager($req);

        $count = Task::where('status', TaskStatus::RUNNING)->count();
        $page = $pager['page'];
        $t = Task::where('status', TaskStatus::RUNNING)->orderBy('reward', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();

        $list = TaskResource::collection($t);
        return res(compact('count', 'page', 'list'));
    }

    public function myList($req): JsonResponse
    {
        $pager = pager($req);

        $count = Task::where('creator_id', Auth::id())->where('status', '!=', TaskStatus::DELETED)->count();
        $page = $pager['page'];
        $t = Task::where('creator_id', Auth::id())->where('status', '!=', TaskStatus::DELETED)->orderBy('reward', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();

        $list = TaskResource::collection($t);
        return res(compact('count', 'page', 'list'));
    }

    public function allList($req): JsonResponse
    {
        $pager = pager($req);

        $count = Task::count();
        $page = $pager['page'];
        $t = Task::orderBy('reward', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();

        $list = TaskResource::collection($t);
        return res(compact('count', 'page', 'list'));
    }

    public function details($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $t = Task::find($id);
        if (!$t) return eRes('Task not found');

        $is_owner = $t->creator_id == Auth::id();
        $in_tc = TaskCompletion::where('task_id', $t->id)->where('completer_id', Auth::id())->count();
        $is_completed = $in_tc > 0;

        $tc = TaskCompletion::where('task_id', $t->id)->get();
        $completers = TaskCompletionResource::collection($tc);

        $details = new TaskResource($t);

        return res(compact('is_owner', 'is_completed', 'completers', 'details'));
    }

    public function create($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'title' => 'required',
            'category' => 'required',
            'description' => 'required',
            'has_attachment' => 'required',
            'slots' => 'required|numeric|min:1',
            'reward' => 'required|numeric|min:1',
        ]);
        if ($v->fails()) return vRes($v);

        if ($req->has('link') && strlen($req->link) > 0) {
            $v = Validator::make($req->all(), [
                'link' => 'url',
            ]);
            if ($v->fails()) return vRes($v);
        }

        $summary = (new LedgerRepository())->mySummary()->getData(true);
        $budget = $req->slots * $req->reward;
        $budget = $budget + ($budget * Settings::TASK_CHARGE_PERCENTAGE);
        if ($budget > $summary['data']['available']) return eRes('Not enough available');

        $uid = Auth::id();
        $t = new Task;
        $t->creator_id = $uid;
        $t->title = $req->title;
        $t->category = $req->category;
        $t->description = $req->description;
        $t->link = $req->link;
        $t->has_attachment = $req->has_attachment == 1 ? 1 : 0;
        $t->expiration_date = $req->expiration_date;
        $t->slots = $req->slots;
        $t->reward = $req->reward;
        $t->status = TaskStatus::RUNNING;
        $t->save();

        $u = User::find($uid);
        (new LedgerRepository())->saveEntry($u, LedgerFlowType::TASK_CREATION, $budget, 'Creating Task ' . $t->title, 1);

        return res();
    }

    public function delete($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $t = Task::find($id);
        if (!$t) return eRes('Task not found');

        $status = $this->getStatus($t);
        if ($status != TaskStatus::RUNNING) return eRes('Task is not running');

        if ($t->creator_id != Auth::id()) return eRes('You are not the owner');

        $this->_finishTask($t);

        $t->status = TaskStatus::DELETED;
        $t->save();

        return res();
    }

    public function end($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $t = Task::find($id);
        if (!$t) return eRes('Task not found');

        $status = $this->getStatus($t);
        if ($status != TaskStatus::RUNNING) return eRes('Task is not running');

        if ($t->creator_id != Auth::id()) return eRes('You are not the owner');

        $this->_finishTask($t);

        $t->status = TaskStatus::COMPLETED;
        $t->save();

        return res();
    }

    public function complete($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $t = Task::where('id', $id)->where('status', TaskStatus::RUNNING)->first();
        if (!$t) return eRes('Task not found');

        $status = $this->getStatus($t);
        if ($status != TaskStatus::RUNNING) return eRes('Task is not running');

        $attempt = TaskCompletion::where('task_id', $t->id)->where('completer_id', Auth::id())->count();
        if ($attempt > 0) return eRes('You already attempted to completed this task');

        if ($t->has_attachment == 1) {
            $v = Validator::make($req->all(), [
                'attachment' => 'required',
            ]);
            if ($v->fails()) return vRes($v);
        }

        $tc = new TaskCompletion;
        $tc->task_id = $t->id;
        $tc->comment = $req->comment;
        $tc->completer_id = Auth::id();
        $tc->save();

        if ($req->attachment != null && strlen($req->attachment) > 0) {
            saveImage($req->attachment, ImageType::ATTACHMENT, $tc->id);
        }

        return res();
    }

    public function reRun($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $t = Task::where('id', $id)->first();
        if (!$t) return eRes('Task not found');

        if ($t->status == TaskStatus::RUNNING) return eRes('Task is still running');

        $summary = (new LedgerRepository())->mySummary()->getData(true);
        $budget = $t->slots * $t->reward;
        $budget = $budget + ($budget * Settings::TASK_CHARGE_PERCENTAGE);
        if ($budget > $summary['data']['available']) return eRes('Not enough available');

        $expiration = now()->addDays(7);

        $newT = new Task;
        $newT->category = $t->category;
        $newT->creator_id = $t->creator_id;
        $newT->title = $t->title;
        $newT->description = $t->description;
        $newT->link = $t->link;
        $newT->has_attachment = $t->has_attachment;
        $newT->expiration_date = $expiration;
        $newT->slots = $t->slots;
        $newT->reward = $t->reward;
        $newT->status = TaskStatus::RUNNING;
        $newT->save();

        $u = User::find($t->creator_id);
        (new LedgerRepository())->saveEntry($u, LedgerFlowType::TASK_CREATION, $budget, 'Re-running Task ' . $t->title, 1);

        $data = new TaskResource($newT);
        return res($data);
    }

    public function getStatus(Task $task): int
    {
        if ($task->status != TaskStatus::RUNNING) return $task->status;
        $expiration = Carbon::parse($task->expiration_date)->addDay();
        $before = now()->isAfter($expiration);
        if ($before) {
            if ($task->status == TaskStatus::RUNNING) {
                $t = Task::find($task->id);
                $t->status = TaskStatus::COMPLETED;
                $t->save();

                $this->_finishTask($task);
            }
            return TaskStatus::COMPLETED;
        }
        return TaskStatus::RUNNING;
    }

    private function _finishTask(Task $task)
    {
        $tc = TaskCompletion::where('task_id', $task->id)->where('status', 1)->get();
        foreach ($tc as $item) {
            $u = User::find($item->completer_id);
            (new LedgerRepository())->saveEntry($u, LedgerFlowType::TASK_COMPLETION, $task->reward, 'Successful completion of task', 1);
        }
        $u = User::find($task->creator_id);
        $reimbursement = ($task->reward * ($task->slots - count($tc)));
        (new LedgerRepository())->saveEntry($u, LedgerFlowType::TASK_REIMBURSEMENT, $reimbursement, 'Reimbursed from unused task slots', 1);
    }

    public function revoke($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'task_hashid' => 'required',
            'completer_hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->task_hashid, 'uuid');
        $t = Task::find($id);
        if (!$t) return eRes('Task not found');

        $status = $this->getStatus($t);
        if ($status != TaskStatus::RUNNING) return eRes('Task is not running');

        if ($t->creator_id != Auth::id()) return eRes('You are not the owner');

        $tc_id = decode($req->completer_hashid, 'uuid');
        $tc = TaskCompletion::find($tc_id);
        if (!$tc) return eRes('Task Completion not found');

        $tc->status = 0;
        $tc->save();

        return res();
    }
}
