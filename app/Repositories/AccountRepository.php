<?php


namespace App\Repositories;


use App\Http\Resources\AccountReferralCodeResource;
use App\Http\Resources\UserAccountResource;
use App\Interfaces\AccountInterface;
use App\Models\Account;
use App\Models\Level;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AccountRepository implements AccountInterface
{
    public function create(int $user_id, string $name, int $referrer_id = 0, int $status = 0, string $remarks = ''): Account
    {
        $aid = encode(now()->timestamp, 'aid');
        $same_aid = Account::where('aid', $aid)->count();
        $has = true;
        while ($has) {
            if ($same_aid > 0) {
                $aid = encode(now()->timestamp + rand(), 'aid');
                $same_aid = Account::where('aid', $aid)->count();
            } else {
                $has = false;
            }
        }

        $a = new Account;
        $a->user_id = $user_id;
        $a->name = $name;
        $a->aid = $aid;
        $a->referrer_id = $referrer_id;
        $a->status = $status;
        $a->remarks = $remarks;
        $a->save();

        $status = 0;
        $acc_count = Account::where('user_id', $user_id)->count();
        if ($acc_count > 1) {
            $status = 1;
        }

        $l = new Level;
        $l->level = 1;
        $l->account_id = $a->id;
        $l->current_referrals = 0;
        $l->current_purchases = 0;
        $l->status = $status;
        $l->save();

        return $a;
    }

    public function getStatus(): JsonResponse
    {
        $uid = Auth::id();
        $a = Account::where('user_id', $uid)->where('status', 1)->get();

        $levels = [];
        for ($i = 1; $i <= 7; $i++) {
            $positions = [];
            $total = 0;

            $l = Level::where('level', $i)->where('status', 1)->pluck('account_id')->toArray();
            $total = count($l);
            foreach ($a as $acc) {
                $pos = array_search($acc->id, $l);
                if ($pos !== false) {
                    array_push($positions, $pos + 1);
                }
            }

            $levels[$i] = [
                'total' => $total,
                'positions' => $positions,
            ];
        }

        return res($levels);
    }

    public function getReferralCodes(): JsonResponse
    {
        $uid = Auth::id();
        $a = Account::where('user_id', $uid)->where('status', 1)->get();

        $data = AccountReferralCodeResource::collection($a);
        return res($data);
    }

    public function users($req): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid permission');

        $pager = pager($req);

        $count = User::count();
        $u = User::orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();

        $list = UserAccountResource::collection($u);
        $page = $pager['page'];

        return res(compact('list', 'count', 'page'));
    }

    public function changeActiveStatus($req): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid permission');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        if ($id == Auth::id()) return eRes('You cant change your account');

        $u = User::find($id);
        if (!$u) return eRes('User not found');

        $u->status = $u->status == 1 ? 0 : 1;
        $u->save();

        return res();
    }

    public function changeBanStatus($req): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid permission');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        if ($id == Auth::id()) return eRes('You cant change your account');

        $u = User::find($id);
        if (!$u) return eRes('User not found');

        $u->ban = $u->ban == 1 ? 0 : 1;
        $u->save();

        return res();
    }
}
