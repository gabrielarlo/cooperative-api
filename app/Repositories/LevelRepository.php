<?php


namespace App\Repositories;


use App\Abstracts\LedgerFlowType;
use App\Abstracts\Settings;
use App\Http\Resources\LevelResource;
use App\Interfaces\LevelInterface;
use App\Models\Account;
use App\Models\Level;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class LevelRepository implements LevelInterface
{

    public function levelUp(Level $level)
    {
        $new_level = $level->level + 1;

        $l = Level::find($level->id);
        $l->level = $new_level;
        $l->current_referrals = 0;
        $l->current_purchases = 0;
        $l->save();

        $account_ids = Account::where('user_id', Auth::id())->pluck('id');
        Level::whereIn('account_id', $account_ids)->update(['current_purchases' => 0]); // Purchases only

        $bonus = 0;
        if ($new_level == 2) {
            $bonus = Settings::LEVEL_TWO_BONUS;
        } elseif ($new_level == 3) {
            $bonus = Settings::LEVEL_THREE_BONUS;
        } elseif ($new_level == 4) {
            $bonus = Settings::LEVEL_FOUR_BONUS;
        }

        if ($bonus > 0) {
            $a = Account::find($level->account_id);
            $u = User::find($a->user_id);
            (new LedgerRepository())->saveEntry($u, LedgerFlowType::LEVEL_BONUS, $bonus, 'For leveling up to ' . $new_level, 1);
        }
    }

    public function list($req): JsonResponse
    {
        $levels = [];
        for ($i = 1; $i <= 7; $i++) {
            $l = Level::where('level', $i)->where('status', 1)->get();
            $data = LevelResource::collection($l);
            $levels[$i] = $data;
        }

        return res($levels);
    }
}
