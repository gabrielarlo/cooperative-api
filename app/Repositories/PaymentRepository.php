<?php


namespace App\Repositories;


use App\Http\Resources\PaymentResource;
use App\Interfaces\PaymentInterface;
use App\Models\PaymentMethod;
use Illuminate\Http\JsonResponse;

class PaymentRepository implements PaymentInterface
{

    public function list(): JsonResponse
    {
        $p = PaymentMethod::where('status', 1)->get();

        $data = PaymentResource::collection($p);
        return res($data);
    }
}
