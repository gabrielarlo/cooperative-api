<?php


namespace App\Repositories;


use App\Abstracts\ImageType;
use App\Http\Resources\AnnouncementResource;
use App\Interfaces\AnnouncementInterface;
use App\Models\Announcement;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AnnouncementRepository implements AnnouncementInterface
{
    public function list($req): JsonResponse
    {
        $a = Announcement::where('status', 1)->orderBy('created_at', 'desc')->limit(10)->get();

        $data = AnnouncementResource::collection($a);
        return res($data);
    }

    public function add($req): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid permission');

        $v = Validator::make($req->all(), [
            'title' => 'required',
            'subject' => 'required',
            'content' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $a = new Announcement;
        $a->creator_id = Auth::id();
        $a->title = $req->title;
        $a->subject = $req->subject;
        $a->content = $req->content;
        $a->save();

        if ($req->image_base_64 != null) {
            saveImage($req->image_base_64, ImageType::ANNOUNCEMENT, $a->id);
        }

        return res();
    }

    public function delete($req): JsonResponse
    {
        if (!isAdmin()) return eRes('Invalid permission');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $a = Announcement::find($id);
        if (!$a) return eRes('Announcement not found');

        $a->status = 0;
        $a->save();

        return res();
    }
}
