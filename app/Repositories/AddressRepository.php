<?php


namespace App\Repositories;


use App\Http\Resources\AddressResource;
use App\Interfaces\AddressInterface;
use App\Models\Address;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AddressRepository implements AddressInterface
{

    public function list(): JsonResponse
    {
        $a = Address::where('user_id', Auth::id())->where('status', 1)->orderBy('default', 'desc')->get();

        $data = AddressResource::collection($a);
        return res($data);
    }

    public function add($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'bldg' => 'required',
            'street' => 'required',
            'barangay' => 'required',
            'city' => 'required',
            'sector' => 'required',
            'contact' => 'required',
            'default' => 'required|min:0|max:1',
        ]);
        if ($v->fails()) return vRes($v);

        $a = new Address;
        $a->user_id = Auth::id();
        $a->bldg = $req->bldg;
        $a->street = $req->street;
        $a->barangay = $req->barangay;
        $a->city = $req->city;
        $a->sector = $req->sector;
        $a->contact = $req->contact;
        $a->default = $req->default;
        $a->note = $req->note ?? '';
        $a->status = 1;
        $a->save();

        return res();
    }

    public function update($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'hashid' => 'required',
            'bldg' => 'required',
            'street' => 'required',
            'barangay' => 'required',
            'city' => 'required',
            'sector' => 'required',
            'contact' => 'required',
            'default' => 'required|min:0|max:1',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $a = Address::find($id);
        if (!$a) return eRes('Address not found');

        if ($a->user_id != Auth::id()) return eRes('You are not the owner');

        if ($req->default == 1) {
            Address::where('user_id', Auth::id())->where('default', 1)->update(['default' => 0]);
        }

        $a->bldg = $req->bldg;
        $a->street = $req->street;
        $a->barangay = $req->barangay;
        $a->city = $req->city;
        $a->sector = $req->sector;
        $a->contact = $req->contact;
        $a->default = $req->default;
        $a->save();

        return res();
    }

    public function delete($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $a = Address::find($id);
        if (!$a) return eRes('Address not found');

        if ($a->user_id != Auth::id()) return eRes('You are not the owner');

        $a->status = 0;
        $a->save();

        return res();
    }

    public function makeDefault($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v);

        $id = decode($req->hashid, 'uuid');
        $a = Address::find($id);
        if (!$a) return eRes('Address not found');

        if ($a->user_id != Auth::id()) return eRes('You are not the owner');

        Address::where('user_id', Auth::id())->where('status', 1)->update(['default' => 0]);
        $a->default = 1;
        $a->save();

        return res();
    }

    public function getDefault(): JsonResponse
    {
        $a = Address::where('user_id', Auth::id())->where('status', 1)->where('default', 1)->first();
        if (!$a) return eRes('No default address');

        $data = new AddressResource($a);
        return res($data);
    }
}
