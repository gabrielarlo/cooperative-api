@component('mail::message')
# Email Verification

Good day {{ $user->name }},<br>
Use this Token for email verification to continue.

<div style="text-align: center; letter-spacing: 4px; text-decoration: underline;">
    <b>{{ $user->verification_token }}</b>
</div>

Thanks,<br>
{{ config('app.name') }} Team
@endcomponent
