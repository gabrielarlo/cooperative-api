<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaskCompletionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('task_completions')) {
            Schema::create('task_completions', function (Blueprint $table) {
                $table->id();
                $table->integer('task_id')->unsigned();
                $table->integer('completer_id')->unsigned();
                $table->string('attachment_name')->nullable()->default(null);
                $table->integer('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_completions');
    }
}
