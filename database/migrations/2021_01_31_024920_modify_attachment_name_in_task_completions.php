<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyAttachmentNameInTaskCompletions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('task_completions', 'attachment_name')) {
            Schema::table('task_completions', function (Blueprint $table) {
                $table->renameColumn('attachment_name', 'comment');
            });
        }
        if (Schema::hasColumn('task_completions', 'comment')) {
            Schema::table('task_completions', function (Blueprint $table) {
                $table->text('comment')->default(null)->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('task_completions', function (Blueprint $table) {
            //
        });
    }
}
