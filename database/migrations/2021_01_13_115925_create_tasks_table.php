<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tasks')) {
            Schema::create('tasks', function (Blueprint $table) {
                $table->id();
                $table->integer('creator_id')->unsigned();
                $table->string('title');
                $table->text('description');
                $table->string('link')->nullable()->default(null);
                $table->integer('has_attachment')->default(0);
                $table->dateTime('expiration_date')->nullable()->default(null);
                $table->integer('slots')->default(1);
                $table->double('reward');
                $table->integer('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
