<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->string('email')->unique();
                $table->string('contact')->nullable()->default(null);
                $table->timestamp('email_verified_at')->nullable();
                $table->string('password');
                $table->string('gender')->nullable()->default(null);
                $table->integer('role')->default(0);
                $table->integer('ban')->default(0);
                $table->integer('status')->default(0);
                $table->string('verification_token')->nullable()->default(null);
                $table->rememberToken();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
