<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('accounts')) {
            Schema::create('accounts', function (Blueprint $table) {
                $table->id();
                $table->integer('user_id')->unsigned();
                $table->integer('referrer_id')->unsigned();
                $table->string('name');
                $table->string('aid');
                $table->integer('encoded')->default(0);
                $table->integer('ec_qualified')->default(0);
                $table->string('aim_id')->nullable()->default(null);
                $table->string('aim_sec_pin')->nullable()->default(null);
                $table->text('remarks');
                $table->integer('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
