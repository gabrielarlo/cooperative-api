<?php

namespace Database\Seeders;

use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SeedPaymentMethods extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'GCash', 'instruction' => 'Send on this number and upload your screenshot as proof of transaction including reference number.'],
            ['name' => 'Coins.ph', 'instruction' => 'Send on this address and upload your screenshot as proof of transaction including reference number.'],
            ['name' => 'BDO', 'instruction' => 'Send on this account number and upload your screenshot as proof of transaction including reference number.'],
            ['name' => 'PayMaya', 'instruction' => 'Send on this number and upload your screenshot as proof of transaction including reference number.'],
            ['name' => 'COD', 'instruction' => ''],
        ];

        foreach ($items as $item) {
            PaymentMethod::firstOrCreate(['slug' => Str::slug($item['name'])], ['name' => $item['name'], 'account' => '', 'instruction' => $item['instruction']]);
        }
    }
}
