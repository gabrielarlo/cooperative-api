<?php

use App\Http\Controllers\API\AccountController;
use App\Http\Controllers\API\AddressController;
use App\Http\Controllers\API\AnnouncementController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\LedgerController;
use App\Http\Controllers\API\LevelController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\PaymentController;
use App\Http\Controllers\API\ReferralController;
use App\Http\Controllers\API\ShopController;
use App\Http\Controllers\API\TaskController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    return res(null, 'API v1 is running...');
});

Route::get('/unauthenticated', function () {
    return eRes('Unauthenticated',401);
})->name('unauthenticated');

Route::group(['prefix' => 'auth'], function () {
    Route::post('/check-referral-code', [AuthController::class, 'checkReferralCode']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/verify-token', [AuthController::class, 'verifyToken']);
    Route::post('/request-password-reset', [AuthController::class, 'requestPasswordReset']);
    Route::post('/reset-password', [AuthController::class, 'resetPassword']);
});

Route::group(['prefix' => 'shop'], function () {
    Route::post('/sale-list', [ShopController::class, 'saleList']);
    Route::post('/shop-list', [ShopController::class, 'shopList']);
    Route::post('/details', [ShopController::class, 'details']);
    Route::post('/get-banners', [ShopController::class, 'getBanners']);
    Route::post('/get-categories', [ShopController::class, 'getCategories']);
    Route::post('/get-reviews', [ShopController::class, 'getReviews']);
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('/logout', [AuthController::class, 'logout']);
        Route::post('/activate-account', [AuthController::class, 'activateAccount']);
        Route::post('/toggle-ban', [AuthController::class, 'toggleBan']);
        Route::post('/update', [AuthController::class, 'update']);
        Route::post('/change-avatar', [AuthController::class, 'changeAvatar']);
        Route::post('/change-cover', [AuthController::class, 'changeCover']);
        Route::post('/change-password', [AuthController::class, 'changePassword']);
        Route::post('/check-auth', function () {
            return res(null,'You are authenticated...');
        });
    });

    Route::group(['prefix' => 'ledger'], function () {
        Route::post('/my-summary', [LedgerController::class, 'mySummary']);
        Route::post('/summary', [LedgerController::class, 'summary']);
        Route::post('/list', [LedgerController::class, 'list']);
        Route::post('/general', [LedgerController::class, 'general']);
    });

    Route::group(['prefix' => 'task'], function () {
        Route::post('/list', [TaskController::class, 'list']);
        Route::post('/my-list', [TaskController::class, 'myList']);
        Route::post('/all-list', [TaskController::class, 'allList']);
        Route::post('/details', [TaskController::class, 'details']);
        Route::post('/create', [TaskController::class, 'create']);
        Route::post('/delete', [TaskController::class, 'delete']);
        Route::post('/end', [TaskController::class, 'end']);
        Route::post('/complete', [TaskController::class, 'complete']);
        Route::post('/re-run', [TaskController::class, 'reRun']);
        Route::post('/revoke', [TaskController::class, 'revoke']);
    });

    Route::group(['prefix' => 'shop'], function () {
        Route::post('/list', [ShopController::class, 'list']);
        Route::post('/add', [ShopController::class, 'add']);
        Route::post('/update', [ShopController::class, 'update']);
        Route::post('/get-images', [ShopController::class, 'getImages']);
        Route::post('/upload-image', [ShopController::class, 'uploadImage']);
        Route::post('/delete-image', [ShopController::class, 'deleteImage']);
        Route::post('/cart-list', [ShopController::class, 'cartList']);
        Route::post('/add-to-cart', [ShopController::class, 'addToCart']);
        Route::post('/sub-from-cart', [ShopController::class, 'subFromCart']);
        Route::post('/remove-from-cart', [ShopController::class, 'removeFromCart']);
        Route::post('/add-to-checkout', [ShopController::class, 'addToCheckout']);
        Route::post('/order', [ShopController::class, 'order']);
        Route::post('/validate-payment', [ShopController::class, 'validatePayment']);
        Route::post('/toggle-stock-status', [ShopController::class, 'toggleStockStatus']);
        Route::post('/get-delivery-charges', [ShopController::class, 'getDeliveryCharges']);
    });

    Route::group(['prefix' => 'address'], function () {
        Route::post('/list', [AddressController::class, 'list']);
        Route::post('/add', [AddressController::class, 'add']);
        Route::post('/update', [AddressController::class, 'update']);
        Route::post('/delete', [AddressController::class, 'delete']);
        Route::post('/make-default', [AddressController::class, 'makeDefault']);
        Route::post('/get-default', [AddressController::class, 'getDefault']);
    });

    Route::group(['prefix' => 'payment'], function () {
        Route::post('/list', [PaymentController::class, 'list']);
    });

    Route::group(['prefix' => 'account'], function () {
        Route::post('/get-status', [AccountController::class, 'getStatus']);
        Route::post('/get-referral-codes', [AccountController::class, 'getReferralCodes']);
        Route::post('/users', [AccountController::class, 'users']);
        Route::post('/change-active-status', [AccountController::class, 'changeActiveStatus']);
        Route::post('/change-ban-status', [AccountController::class, 'changeBanStatus']);
    });

    Route::group(['prefix' => 'referral'], function () {
        Route::post('/list', [ReferralController::class, 'list']);
        Route::post('/top-ten', [ReferralController::class, 'topTen']);
    });

    Route::group(['prefix' => 'order'], function () {
        Route::post('/list', [OrderController::class, 'list']);
        Route::post('/my-list', [OrderController::class, 'myList']);
        Route::post('/update-status', [OrderController::class, 'updateStatus']);
        Route::post('/product-list', [OrderController::class, 'productList']);
        Route::post('/product-review', [OrderController::class, 'productReview']);
    });

    Route::group(['prefix' => 'announcement'], function () {
        Route::post('/list', [AnnouncementController::class, 'list']);
        Route::post('/add', [AnnouncementController::class, 'add']);
        Route::post('/delete', [AnnouncementController::class, 'delete']);
    });

    Route::group(['prefix' => 'level'], function () {
        Route::post('/list', [LevelController::class, 'list']);
    });
});
