<?php

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Intervention\Image\Facades\Image;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return res(null,'Hello there!');
});

Route::get('/product/thumbnail/{filename}', function ($filename) {
    $files = File::allFiles(public_path('product'));
    foreach ($files as $file) {
        if ($filename == $file->getFilename()) {
            $img = Image::make($file->getRealPath())->resize(320, null, function ($constraints) {
                $constraints->aspectRatio();
            });
            return $img->response();
        }
    }
    $img = Image::canvas(320, 320, '#ff0000');
    return $img->response();
});
